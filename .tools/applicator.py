import subprocess

# Open reapply.patch3
f=open("reapply.patch3", "r")
subprocess.call("cd ../../server-code", shell=True)

for line in f:
    if line[0] == "#" or line[0] == "\r" or line[0] == "\n":
        continue
    print "Downloading patch "+line.replace("\n", "")
    subprocess.call("cd ../../server-code ; wget https://gitlab.com/evol/hercules/commit/"+line.replace("\n", "")+".diff", shell=True)
    print "Applying patch..."
    #subprocess.call("cd ../../server-code ; ls", shell=True)
    subprocess.call("cd ../../server-code ; git apply --ignore-whitespace --reject "+line.replace("\n", "")+".diff", shell=True)
    print "Patch applied"
    subprocess.call("cd ../../server-code ; rm "+line.replace("\n", "")+".diff", shell=True)
    print "Patch deleted (success)"


f.close()
print "Evol porting finished"


###############################################
# Open reapply.patch4 (HerculesWS Upstream)
f=open("reapply.patch4", "r")
subprocess.call("cd ../../server-code", shell=True)

for line in f:
    if line[0] == "#" or line[0] == "\r" or line[0] == "\n":
        continue
    print "Downloading patch "+line.replace("\n", "")
    subprocess.call("cd ../../server-code ; wget https://github.com/HerculesWS/Hercules/commit/"+line.replace("\n", "")+".diff", shell=True)
    print "Applying patch..."
    #subprocess.call("cd ../../server-code ; ls", shell=True)
    subprocess.call("cd ../../server-code ; git apply --ignore-whitespace --exclude=doc/ --exclude=tools/ --exclude=db/ --exclude=npc/ --exclude=conf/ --reject "+line.replace("\n", "")+".diff", shell=True)
    print "Patch applied"
    subprocess.call("cd ../../server-code ; rm "+line.replace("\n", "")+".diff", shell=True)
    print "Patch deleted (success)"


f.close()
print "HercWS porting finished"

