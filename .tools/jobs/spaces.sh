#!/bin/bash

find -H . -type f -name "*.txt" -exec sed -i 's/[[:blank:]]*$//' {} \;

export RES=$(git diff --name-only)
if [[ -n "${RES}" ]]; then
    echo "Extra spaces before new lines detected in files:"
    git diff --name-only
    # I don't want this to be critical on TMW2
    exit 1
fi
