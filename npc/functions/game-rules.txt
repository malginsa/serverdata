// TMW-2 script.
// Evol scripts.
// Authors:
//    gumi
//    Qwerty Dragon
//    Reid
//    WildX
//    Jesusalva
// Description:
//    Main Rules of TMW2.

function	script	GameRules	{
    /*
    if (checkweight(BookOfLaws, 1) == true && countitem(BookOfLaws) < 1)
        getitem BookOfLaws, 1; // give the book if the player lost it
    */

    narrator getarg(0, 0),
        //l("1. ##BDo not bot##b, A character must be being controlled by a human, standing, siting, or logged off. You may only control one character at a time."),
        //l("* You can use a BOT if it was provided by TMW2 Team. In no circumstance you shall AFK-Bot, or you'll be promptly banned or jailed."),
        l("1. You must be at the computer to play. Using a bot ##Bwhile at computer##b will be tolerated."),
        l("Failure to respond to other players and/or GMs in a timely manner will be viewed as unacceptable AFK botting. And you'll spend a night in the jail."),
        l("2. ##BDo not spam nor flood.##b This rule is not limited to public chat, actions ingame can be flooding too."),
        l("3. ##BDo not trade invalid items, or try to cheat on trades.##b This includes any other kind of cheat or bug abuse, passive of account deletion and IP ban as stated by the Terms Of Service."),
        l("4. ##BRespect other players.##b This includes but is not limited to using offensive language in nicknames or chat, and begging items or favours to other players."),
        l("5. ##BThe public chat is to be understood by everyone.##b Therefore, try to use english when possible."),
        l("6. ##BDo not create multi accounts.##b A person may only hold one account and as many chars as allowed by the server/client. Staff members with special privileges in-game may have a second account without those privileges."),
        l("7. ##BAs long as you have an account, you agree with the [@@https://tmw2.org/legal|Terms Of Service@@]##b. Shall any rule conflict with these terms, the Terms Of Service provided on that link shall prevail."),
        l("8. ##BThe use of real money is prohibited##b for ingame stuff, except by sponsoring."),
        l("9. ##BAdmit when you're wrong.##b Users trying to lie to or fool GMs will get no pity from them."),
        l("10. ##BDo not logout##b at Botcheck area or at Jail. We cannot unjail an offline player. The opposite of rule 9: If you believe you're right, keep your ground and explain calmly what happened. We'll calmly analyse the situation. If you were jailed without guilt, an apology will be sent to you, provided this rule is not broken."),
        l("11. If you believe you are Not Guilty, but the GM deemed you guilty, request a ##BCouncil Trial##b. It's your right. In a Council Trial, we'll have multiple people giving the veredict. Any player may be called to compose the council. All admins shall take part in it. An arragment relative to time, date, and number of counsellors is to be made.");
    return;
}


-	script	@rules	32767,{
    end;

    function read_book {
        narrator S_FIRST_BLANK_LINE | S_LAST_NEXT,
            l("This book outlines the laws which applies everywhere in the World Of Mana."),
            l("The first page contains the universal rules that have been agreed upon throughout the land.");

        GameRules S_NO_NPC_NAME | S_LAST_NEXT;

        narrator S_NO_NPC_NAME,
            l("The next page begins to list the complex trading laws and political rules. The word \"%s\" shows up everywhere.", "[@@https://gitlab.com/TMW2|@@]"),
            l("All this seems unimportant to you right now.");
        if (BaseLevel > 30) {
            mesc l("However, there's a link to get a simple python bot software!");
            mes "@@https://gitlab.com/TMW2/evol-all/tmw2_bot.py|Simple Python Bot (Linux/Windows/Mac)@@";
        }
        close;
    }

OnCall:
    GameRules;
    close;

OnUseBook:
    if (openbook())
        read_book;
    bye;

OnShelfUse:
    if (openbookshelf())
        read_book;
    bye;

OnInit:
    .book_name$ = "The Book of Laws";
    bindatcmd "rules", "@rules::OnCall", 0, 2, 0;
}
