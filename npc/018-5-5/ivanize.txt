// TMW2 scripts.
// Authors:
//    Indigovox <rui.gravata@hotmail.com>
//    Jesusalva <jesusalva@tmw2.org>
// Description:
//    Evil Fairy

018-5-5,29,38,0	script	Ivanize	NPC_FAIRY_C,{
    function ivanizeBusy;
    if (strcharinfo(0) == "Manatauro") npctalk3 l("Leave me alone! Stop annoying me!");
    .@q = getq(LilitQuest_Ivanize);
    if (BaseLevel < 50) ivanizeBusy();
    if (.@q == 0) goto L_FirstTime;
    if (.@q > 0) goto L_Exchange;
    ivanizeBusy();
    end;

L_FirstTime:
    mesn;
    mesq l("Hello %s.", strcharinfo(0));
    next;
    mesn strcharinfo(0);
    select
        l("And who are you?"),
        l("How do you know my name?");
    mes "";
    mesn;
    mesq l("My name is %s. I am an upstanding citizen of this kingdom. Everyone loves me.", .name$);
    next;
    mesn;
    mesq l("Because I am a special nymph.");
    mes "%%0";
    next;
    mesn strcharinfo(0);
    select
        l("Hahah, tell me another one!"),
        l("Let's pretend I believe you.");
    mes "";
    mes "%%:";
    mes "";
    mesc l("%s, a bit upset, tries to change subjects.", .name$);
    next;
    mesn;
    mesq l("Have you brought me some %s?", getitemlink(SnakeEgg));
    next;
    mesn strcharinfo(0);
    select
        l("Why you don't tell me?"),
        l("Wait, I thought you were a nymph!");
    mes "";
    mesn;
    mesq l("I know you have %d %s with you, just haven't said before to be polite.", countitem(SnakeEgg), getitemlink(SnakeEgg));
    next;
    mesn;
    mesq l("I like to eat snake eggs. Lemme do you a proposal: You bring me %d %s, and I'll give you a flask of %s.", .minEggs, getitemlink(SnakeEgg), getitemlink(NymphPoison));
    next;
    mesn;
    mesq l("Just make sure to don't try to poison a guard or something, and don't tell anyone of our little deal. As a proof of our agreement, I'll even give you a freebie, so you can see this is the real deal.");
    next;
    mesn;
    mesq l("Come back when you have the Snake Eggs for me.");
    getitem NymphPoison, 1;
    setq LilitQuest_Ivanize, 1;
    close;

L_Exchange:
    .@c=countitem(SnakeEgg);
    .@o=(.@c/.minEggs);
    if (.@c < .minEggs) ivanizeBusy();
    mesn;
    mesq l("Look at this, %s really brought me %d %s.",
            strcharinfo(0), countitem(SnakeEgg), getitemlink(SnakeEgg));
    next;
    mesn strcharinfo(0);
    select
        l("I want %d %s!", .@o, getitemname(NymphPoison)),
        l("I'll see you later.");
    mes "";
    if (@menu == 2) close;
    delitem SnakeEgg, .@o*.minEggs;
    getitem NymphPoison, .@o;
    compareandsetq LilitQuest_Ivanize, 1, 2;
    mesn;
    mesq l("Here you go; See you later!");
    close;

function ivanizeBusy {
    mesn;
    mesq l("Can't you see I'm busy?");
    close;
}

OnInit:
    .minEggs = 15;
    .sex=G_FEMALE;
    .distance=5;
    end;
}

