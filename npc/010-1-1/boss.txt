// TMW2 Script
// Author:
//  Jesusalva
// Description:
//  Giant Cave Maggot Boss

010-1-1,0,0,0	script	#BossCtrl_010-1-1	NPC_HIDDEN,{
    end;

// Respawn every hour
OnTimer3600000:
    stopnpctimer;
OnInit:
    // WARNING: (51,19) INVALID COORDINATES
    setarray .xp,  49, 51,  44, 101, 124, 164, 132, 154, 116;
    setarray .yp,  59, 19, 100,  45,  26,  35,  61, 108,  94;
    .@tg=rand(getarraysize(.xp)-1);
    monster "010-1-1", .xp[.@tg], .yp[.@tg], strmobinfo(1, GiantCaveMaggot), GiantCaveMaggot, 1, "#BossCtrl_010-1-1::OnBossDeath";
    end;

OnBossDeath:
    initnpctimer;
    .@party=getcharid(1);
    if (.@party > 0) {
        mapannounce getmap(), "Boss deafeated by Party: " + getpartyname(.@party), bc_all;
    } else {
        mapannounce getmap(), "Boss deafeated by: " + strcharinfo(0), bc_all;
    }
    fix_mobkill(GiantCaveMaggot);
    end;

}
