// TMW2 Script
// Author:
//  Jesusalva
// Description:
//  Alchemy System (Player, Guild, NPC)
// Notes:
//  Base for Evol MR

// Usage: AlchemySystem ({scope})
// Scopes: CRAFT_NPC, CRAFT_PLAYER, CRAFT_GUILD
// If an invalid scope is passed, .knowledge won't be set but will be required
// Returns true on success, false on failure
function	script	AlchemySystem	{
    // Set .scope, .knowledge and .success
    .scope=getarg(0, CRAFT_PLAYER);
    if (.scope == CRAFT_PLAYER)
    {
        copyarray(.knowledge,RECIPES_ALCHEMY,getarraysize(RECIPES_ALCHEMY));
    }
    else if (.scope == CRAFT_GUILD)
    {
       copyarray( .knowledge,getd("$RECIPES_ALCHEMY_"+getcharid(2)),getarraysize(getd("$RECIPES_ALCHEMY_"+getcharid(2))) );
    }
    .success=false;

    setskin "craft2";
    .@var$ = requestcraft(2);
    .@craft = initcraft(.@var$);
    .@entry = findcraftentry(.@craft, CRAFT_ALCHEMY);
    if (debug || $@GM_OVERRIDE) mes "found craft entry: " + .@entry;
    if (debug || $@GM_OVERRIDE) mes "knowledge value: " + .knowledge[.@entry];
    if (.@entry < 0) {
            .success=false;
    } else {
        if (.scope == CRAFT_NPC || .knowledge[.@entry]) {
            if (GSET_FIXED_ALCHEMY) {
                .@m=limit(1, GSET_FIXED_ALCHEMY, 25);
            } else {
                .@max=(is_sponsor() ? 25 : 10);
                mesc l("How many to brew? (%d-%d)", 1, .@max);
                input(.@m, 1, .@max);
            }
            // Alchemy loop
            .@i=0;
            while (.@i < .@m) {
                .@s=validatecraft(.@craft);
                // Could not validate (not enough resources)
                if (!.@s) {
                    mesc l("Not crafting - insufficient materials!"), 1;
                    break;
                }
                .@s=usecraft(.@craft);
                .@i++;
                callfunc "FYE_Olympics_AL";
                // Exploiting?!
                if (!.@s)
                    break;
            }
            .success=true;
        } else {
            .success=false;
        }
    }
    deletecraft .@craft;
    setskin "";
    return .success;
}

/*
Alchemy can rely in cross-building
Where a weaker potion is base for a stronger one
Standard Duration = 2 minutes ~ 5 minutes

Reagents:
	Water
	...Eggs?
	...Milk?
	Nymph Poison
	Death Potion
	Manapple

Products:
	Tea (Chamomile, Spearmint, Oolong, Jasmine, Yerba Mate?)
	→ Argaes Water + «Herbal Reagent»
OK	Coffee (Shadow Herb + Tonori Water)
OK	Piberries Infusion (Piberries + Curshroom)
OK	Atropos Mixture (Lachesis Brew + Clotho Liquor)
OK	Death Potion (Dragonfruit + Nightshade Tea)
OK	Smoke Grenade (Cactus pot + Coal)
OK	Grenade (Cactus pot + Sulfur Powder)
OK	Scented Grenade (Cactus pot + Moss)
OK	Haste Potion (Plushshroom)
OK	Strength Potion (Chagashroom)
	Return Potion (Hurnscald Recipe => Ocean Croc Claw + Hard Spike? Grass Seeds?)
OK	Status Reset (Curshroom + Mana Piou Feather)
OK	Homun Stat Reset (Curshroom + Manapple)
OK	Move Speed (Gem Powder + Fluor Powder)
OK	Precision (Piberries + Mt. Snake Egg)
OK	Dodge Potion (Piberries + Snake Egg)
OK	Luck, Dex, Int, Vit, Agi (Gems + Tea)
OK	Sacred Life (Golden Apple + Elixir of Life)
OK	Sacred Mana (Golden Apple + Celestia Tea)
OK	Sacred Revival (Sacred Life + Sacred Mana)
OK	Broken Warp Crystal? (Wurtizite + Black Mamba Skin)
OK	Magic Apple? (Divine Apple + Manapple? Death Potion? Sacred Life/Revival?)
OK	Purification Potion (Nymph Poison + Sacred Life)
OK	Iced Bottle (Tonori W. + Argaes W.)
OK	Insurance Contract (» Insurance?) (Quill + Reed Bundle)
OK	Insurance (Quill + Death Potion)
	Mysterious Fruit? (Legendary)

For all Scrolls: Quill + ? (depends on scroll itself)
	» Summon Scrolls (Based on mob parts, 1× mob?)
		→ alignment_cansummon() + SummonMagic() or summon() directly
		→ Criteria between weak/strong version is alignment
		→ When aligned, scrolls always summon strongest ver
		→ Level must be equal or superior to strongest, tho
		» Maggot/Giant Maggot: Bug Leg (Lv 40)
		» CaveMaggot: Maggot Slime
		» Green Dragon/Nightmare: Dragon Scales (Lv 105)
		» Wolvern: Wolvern Pelt
		» Moggun/Yeti: Frozen Yeti Tear (Lv 60)
		» Terranite/T.Prot.: Terranite Ore (Lv 90)
	» Magnus Heal (Lifestone)
	» Area Provoke? → Scent grenade?
	» Guild Skills?
OK		» ScrollAngelLightA ( + )
OK		» ScrollBattlePlansA ( + )
OK		» ScrollDefenseBlessA ( + )
OK		» ScrollCriticalFortuneA ( + )
	→ TODO: Kyrie Eleison (Absolute Shield)
	→ With self-stun, makes you a temporary wall?
	→ Maybe a item of Quill + LoF Coin for guild skills? (LoF Quill)

		// Skills for Aegis Shield, all beyond maximum level
		// Slimes, Snakes, Fairies, Darth Duck, Mr. Prickel
		// PoisonS.Mushroom
		// TODO: Lizards, (Black)Scorpions, Moonshroom, Black Mamba, Centaur
		skill TMW2_HALHISS, 10;
		skill TMW2_KALSPIKE, 9;
		skill TMW2_LIMERIZER, 10;
		skill TMW2_FAIRYKINGDOM, 9;
		skill TMW2_DUCKY, 10;
		skill TMW2_FAIRYEMPIRE, 10;

*/

