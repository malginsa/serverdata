// TMW2 scripts.
// Authors:
//    Jesusalva
//    Dusty in a Bottle (aka. Dustynator, Dustman, mr. willbelz)
// Description:
//    Scripts for 001-10: Snipers, Bottles and Warps

// Boss room
001-10,89,43,0	script	#COD_BossRoomCheck	NPC_NO_SPRITE,0,0,{
    end;
OnTouch:
    if (!@COD_CHECKPOINT) {
        dispbottom l("Magic Barrier is active. You must give a whole circle on the desert to break it.");
    } else {
        deltimer("#COD_BossRoomCheck::OnTimeDefeat");
        addtimer(300000,"#COD_BossManager::OnTimeDefeat");
        // You can't return now!
        warp "001-10-1", any(57, 58, 59), any(74, 75);
        dispbottom l("A dangerous boss room, keep your guard up!");
    }
    end;

OnTimeDefeat:
    @COD_CHECKPOINT=0;
    if (getmap() ~= "001-10") {
        warp "018-2-1", 24, 29;
        dispbottom l("COD: Ran out of time!");
    }
    end;

}

// Magic Barrier
001-10,132,106,0	script	#COD_Checkpoint106	NPC_NO_SPRITE,0,0,{
    end;
OnTouch:
    getmapxy(.@m$, .@x, .@y, 0);
    slide .@x+2, .@y;
    if (!@COD_CHECKPOINT)
        dispbottom l("Checkpoint! Magic Barrier went down!");
    @COD_CHECKPOINT=1;
    end;
}

001-10,132,107,0	duplicate(#COD_Checkpoint106)	#COD_Checkpoint107	NPC_NO_SPRITE,0,0
001-10,132,108,0	duplicate(#COD_Checkpoint106)	#COD_Checkpoint108	NPC_NO_SPRITE,0,0
001-10,132,109,0	duplicate(#COD_Checkpoint106)	#COD_Checkpoint109	NPC_NO_SPRITE,0,0
001-10,132,110,0	duplicate(#COD_Checkpoint106)	#COD_Checkpoint110	NPC_NO_SPRITE,0,0
001-10,132,111,0	duplicate(#COD_Checkpoint106)	#COD_Checkpoint111	NPC_NO_SPRITE,0,0
001-10,132,112,0	duplicate(#COD_Checkpoint106)	#COD_Checkpoint112	NPC_NO_SPRITE,0,0
001-10,132,113,0	duplicate(#COD_Checkpoint106)	#COD_Checkpoint113	NPC_NO_SPRITE,0,0

001-10,0,0,0	script	#CODMASTER	NPC_NO_SPRITE,{
    end;
    function spawner { // (Event, x1, y1, x2, y2, Amount, modifier)
        .@ev$=getarg(0, "#CODMASTER::OnDeath");
        .@x1=getarg(1,0);
        .@y1=getarg(2,0);
        .@x2=getarg(3,200);
        .@y2=getarg(4,150);
        .@am=getarg(5,1);
        freeloop(true);
	    for (.@i = 0; .@i < .@am; ++.@i) {
            if (!getarg(6,0))
                .@monsterId=any(DustRifle, DustGatling, DustRevolver);
            else
                .@monsterId=any(AngryBat, Snake, AngryBat, DesertBandit, AngryBat, Sarracenus);
            areamonster("001-10", .@x1, .@y1, .@x2, .@y2, strmobinfo(1, .@monsterId), .@monsterId, 1, .@ev$);

        }
    }

// Death handlers
OnWLDeath:
    if (playerattached()) {
        if (rand2(0,1000) <= 20)
            getitem BottledDust, 1;
    }
    spawner("#CODMASTER::OnWLDeath", 0, 0, 200, 150, 1, 1);
    end;

OnNDeath:
    if (playerattached()) {
        if (rand2(0,1000) <= 40)
            getitem BottledDust, 1;
    }
    spawner("#CODMASTER::OnNDeath", 0, 10, 200, 25);
    end;

OnSDeath:
    if (playerattached()) {
        if (rand2(0,1000) <= 40)
            getitem BottledDust, 1;
    }
    spawner("#CODMASTER::OnSDeath", 0, 129, 200, 150);
    end;

OnCDeath:
    if (playerattached()) {
        if (rand2(0,1000) <= 40)
            getitem BottledDust, 1;
    }
    spawner("#CODMASTER::OnCDeath", 73, 45, 132, 86);
    end;

OnWDeath:
    if (playerattached()) {
        if (rand2(0,1000) <= 40)
            getitem BottledDust, 1;
    }
    spawner("#CODMASTER::OnWDeath", 10, 25, 72, 122);
    end;

OnEDeath:
    if (playerattached()) {
        if (rand2(0,1000) <= 40)
            getitem BottledDust, 1;
    }
    spawner("#CODMASTER::OnEDeath", 160, 45, 190, 130);
    end;

OnDeath:
    if (playerattached()) {
        if (rand2(0,1000) <= 40)
            getitem BottledDust, 1;
    }
    spawner("#CODMASTER::OnDeath");
    end;

/////////////////////////////////////////////////////////////////////////////////
///              on init block                 ////    on init block      //////
/////////////////////////////////////////////////////////////////////////////////
OnInit:
    // Spawn Wildlife
    spawner("#CODMASTER::OnWLDeath", 0, 0, 200, 150, 12, 1);

    // Spawn other monsters
    spawner("#CODMASTER::OnDeath", 0, 0, 200, 150, 3);
    spawner("#CODMASTER::OnNDeath", 0, 10, 200, 25, 5);
    spawner("#CODMASTER::OnSDeath", 0, 129, 200, 150, 3);
    spawner("#CODMASTER::OnCDeath", 73, 45, 132, 86, 4);
    spawner("#CODMASTER::OnWDeath", 10, 25, 72, 122, 5);
    spawner("#CODMASTER::OnEDeath", 160, 45, 190, 130, 4);

    end;
}

