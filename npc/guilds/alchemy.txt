// Moubootaur Legends Script
// Author:
//    Jesusalva
// Description:
//  Guild Facility - Alchemy Table

guilds,24,35,0	script	Guild Chemistry	NPC_NO_SPRITE,{
    do
    {
        mesc l("What will you brew today?");
        mesc l("Note: Items brewed here will use a Guild Recipe instead!");
        if (AlchemySystem(CRAFT_GUILD))
        {
            mesc l("Success!"), 3;
            next;
        }
        else
        {
            mesc l("That didn't work!"), 1;
            next;
        }
        mesc l("Try again?");
    } while (askyesno() == ASK_YES);
    close;

OnInit:
    .distance=2;
    end;
}

