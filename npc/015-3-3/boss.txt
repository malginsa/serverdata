// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Pirate Den - Boss Fight & Treasure Chest

015-3-3,100,67,0	script	Pirate Treasure Chest	NPC_CHEST,{
    /* Boss Showdown incomplete */
    if (getq(HurnscaldQuest_PirateCave) < 7) {
        warp "015-3-2", 24, 62;
        end;
    }
    /* Treasure Chest logic */
    if (getq(HurnscaldQuest_PirateCave) == 7) {
        // You'll always get the maximum prize the first time
    	TreasureBox(99999);
        Zeny+=rand2(5700, 6200);
        getexp 1000, 320;
        compareandsetq HurnscaldQuest_PirateCave, 7, 8;
    } else {
    	TreasureBox(50); // 0.50% extra chance
    }
	specialeffect(.dir == 0 ? 24 : 25, AREA, getnpcid()); // closed ? opening : closing
	close;
OnInit:
	.distance=2;
	end;
}

// Boss Showdown Intercept
015-3-3,100,62,0	script	#Marley	NPC_HIDDEN,2,0,{
    end;
OnTouch:
    if (getq(HurnscaldQuest_PirateCave) >= 7) end;
    slide 70, 50;
    if (mobcount("015-3-3", "#Marley::OnWin")) end;
    // Cutscene + Boss!
    sleep2(500); // For ManaPlus
    dispbottom l("??? : Wait, how did you got here? WHO ARE YOU?");
    sleep2(2500);
    dispbottom l("??? : Are you trying to steal MY treasure?!");
    sleep2(2500);
    dispbottom l("??? : I am Pirate Captain Marley, and on the honor of my pirates...");
    sleep2(2500);
    dispbottom l("Marley : I shall stop you!");
    // Someone has beat you to the punch!
    if (mobcount("015-3-3", "#Marley::OnWin")) end;
    // Spawn the boss!
    monster("015-3-3", 69, 54, strmobinfo(1, Marley), Marley, 1, "#Marley::OnWin");
    // And the reinforcements...
    areamonster("015-3-3", 62, 47, 81, 57, strmobinfo(1, Thug), Thug, 4);
    areamonster("015-3-3", 62, 47, 81, 57, strmobinfo(1, Swashbuckler), Swashbuckler, 4);
    areamonster("015-3-3", 62, 47, 81, 57, strmobinfo(1, Grenadier), Grenadier, 4);
    initnpctimer;
    end;

// Extra reinforcements if Marley still alive
OnTimer30000:
    if (!mobcount("015-3-3", "#Marley::OnWin")) end;
    areamonster("015-3-3", 62, 47, 81, 57, strmobinfo(1, Thug), Thug, 3);
    areamonster("015-3-3", 62, 47, 81, 57, strmobinfo(1, Swashbuckler), Swashbuckler, 3);
    areamonster("015-3-3", 62, 47, 81, 57, strmobinfo(1, Grenadier), Grenadier, 3);
    end;

OnWin:
    dispbottom l("Marley : You... thief... *grumble*");
    compareandsetq HurnscaldQuest_PirateCave, 6, 7;
    end;
}

// Exit Gate, only usable if Marley isn't around anymore
015-3-3,88,66,0	script	#MarleyOut	NPC_HIDDEN,0,2,{
    end;
OnTouch:
    if (mobcount("015-3-3", "#Marley::OnWin")) end;
    slide 100, 60;
    end;
}


