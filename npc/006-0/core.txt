// TMW2 Script
// Author:
//    Jesusalva

006-0,46,26,0	script	Magic Barrier#0060	NPC_HIDDEN,0,0,{
    end;

OnTouch:
    if (CRAZYPOINTS < 3) goto L_Reject; // FIXME
    /* TODO: Piou Isles Arc */
    if (getq(General_Narrator) < 3) goto L_Reject; // Tulimshar Arc must be finished

    specialeffect(5000, SELF, getcharid(3));
    dispbottom l("The power which lies in Candor acknowledges your strength.");
    sleep2(2500);
    warp "006-3", 49, 52;
    sleep2(500);
    specialeffect(5001, SELF, getcharid(3));
    end;

L_Reject:
    specialeffect(5000, SELF, getcharid(3));
    dispbottom l("The power which lies in Candor rejects your strength.");
    sleep2(3000);
    specialeffect(5002, SELF, getcharid(3));
    end;
}
