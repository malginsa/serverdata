// TMW2 script
// Author: Jesusalva <admin@tmw2.org>
//
// Magic Script: TMW2_REVIVE
// Magic Script: TMW2_RESSURECT
//
// Skill to revive players
// TODO: Reimburse EXP?

// revive target (level, target)
function	script	SK_resurrect	{
    .@lv=getarg(0);
    .@tg=getarg(1);
    .@me=getcharid(3);
    attachrid(.@tg);
    if (ispcdead()) {
        recovery(.@tg);
        percentheal 100, 0;
        percentheal -(100 - 10 * .@lv), 0;
    }
    detachrid();
    attachrid(.@me);
	return;
}

// revives getarg(0)
function	script	SK_revive	{
	.@target=getarg(0);
	if (getunittype(.@target) != UNITTYPE_PC) {
		dispbottom l("This skill can only be used on players!");
		return;
	}
    recovery(.@target);
	return;
}

// revive in getarg(0) range from caster
function	script	SK_ressurect	{
    .@r=getarg(0);
    getmapxy(.@m$, .@x, .@y, 0);
    recovery(.@m$, .@x-.@r, .@y-.@r, .@x+.@r, .@y+.@r);
	return;
}

// revives the whole map [ULTIMATE]
function	script	SK_sanctum	{
    BaseLevel-=1; // Maybe EXP Gain -300% for a hour?
    recovery(getmap());
    //maptimer AUTOREVIVE 15 minutes
	return;
}

