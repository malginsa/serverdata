// All .forum.txt files were not converted
// author: TomphaA


//Alex

009-1.gat,??,??,?|script|Alex|???,{


    if (TOM_Quest == 1) goto L_Alex_Waits_1;
    if (TOM_Quest == 2) goto L_Alex_Tablet;
    if (TOM_Quest == 3) goto L_Alex_Didyou;
    if (TOM_Quest == 4) goto L_Alex_Okay;
    if (TOM_Quest == 5) goto L_Alex_End;


    mes "[Alex]";
    mes "\"Hey there!  Mind giving us a hand?\"";
    menu

        "Yeah, what do you need?", L_Alex_Help,

        "I Don't have time, sorry.", -;

    close;

L_Alex_Help

	mes "[Alex]";
	mes "\"He shows you little wooden box.\"";
    mes "\"Give these to Andy you can find him somewhere around in Dark World, I'm sure he'll reward you.\"";
	set TOM_Quest, 1;
	close;

L_Alex_Tablet:

    mes "[Alex]";
    mes "\"Hey, I heard you have something for me...\"";
	mes "\"You give Alex the stone tablet, Alex looks at it and seems to be very happy\"";
	mes "[Alex]";
	mes "\"I need you to tell Andy that we found what we we're looking for and that we dont have to examine those Dark World creatures anymore, I'm sure he'll understand.\"";
	set TOM_Quest, 3;
    close;

L_Alex_Didyou:

    mes "[Alex]";
    mes "\"Did you tell it to him already, if not please hurry!\"";
    close;

L_Alex_Waits_1:

    mes "[Alex]";
    mes "\"Did you give that box to Andy?\"";
    close;

L_Alex_Okay:

	mes "[Alex]";
    mes "Did you tell him what I asked you to tell him?";
    menu

        "Yes I did, but he asked me to tell you that he is going to stay in Dark World because he likes that place.", L_Alex_Damn,

		"No I didn't", -;

    close;

L_Alex_Damn:

	set TOM_Quest, 5;
    mes "[Alex]";
    mes "\"Ah, thank you. Hope he's mentally okay...\"";
	mes "\"Here is your reward anyways, Andy found these a year ago from Dark World...\"";
	getinventorylist;
    if (@inventorylist_count == 100) goto L_Alex_TooMany;
	getexp 3000, 0;
	set Zeny, Zeny + 4000;
	getitem "Whiterabbitears", 1;
	close;

L_Alex_TooMany:

    mes "[Alex]";
    mes "\"You have too many items for me to give you a reward.\"";
    close;

L_Alex_End:

    mes "[Alex]";
    mes "\"Be safe out there, Dark World is dangerous for the unexperienced!\"";
    close;

}












//Andy

???-?.gat,??,??,?|script|Andy|???,{

    if (TOM_Quest == 1) goto L_Andy_Okay;
    if (TOM_Quest == 2) goto L_Andy_Tablet;
    if (TOM_Quest == 3) goto L_Andy_Tell;
    if (TOM_Quest == 4) goto L_Andy_whatdidhesay;
    if (TOM_Quest == 5) goto L_Andy_End;

    mes "[Andy]";
    mes "\"Lots of work to do.\"";
    close;

L_Andy_Okay:

    mes "[Andy]";
    mes "\"Oh, hello there.\"";
	next;
	mes "\"You show him the wooden box Alex gave you earlier.\"";
	mes "\"Alex wanted you to give me that? Okay, let me see...\"";
	getexp 1000, 0;
	set Zeny, Zeny + 1000;
    goto L_Andy_Help

L_Andy_Help:

    mes "[Andy]";
    mes "\"I need Alex to see this...\"";
	mes "\"Andy shows you a stone tablet.\"";
	mes "\"Please get this to Alex!\"";
	set TOM_Quest, 2;
    close;

L_Andy_Tablet:

    mes "[Andy]";
    mes "\"Did you show Alex that tablet\"";
    close;

L_Andy_Tell

    mes "[Andy]";
    mes "\"What did he say?\"";
	menu

        "He said that you have found what you were looking for and you dont have to stay here in Dark World anymore.", L_Andy_Well,

        "Nothing...", -;

		close;

L_Andy_Well:

    mes "[Andy]";
    mes "\"Thats great news, but tell Alex that I'm going to stay here because I like this place.\"";
	getexp 1000, 0;
	set TOM_Quest, 4;
    close;

L_Andy_Whatdidhesay:

    mes "[Andy]";
    mes "\"Pleasy hurry!\"";
    close;

L_Andy_End:

    mes "[Andy]";
    mes "\"Thanks for your help again!\"";
    close;

}
