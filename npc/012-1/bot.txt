// TMW2 Script
// Author:
//    Jesusalva
// Description:
//    Introduces the new area ingame
// Quest:
//    TODO: Quest, requires 1× LOFCoin (obtain for free with Pyndragon)
//    Reward: Mouboo Figurine

012-1,82,59,0	script	LOFBot	NPC_PLAYER,{
    .@q=getq(HurnscaldQuest_LOFPass);
    if (rand2(1,5) % 2 == 1) {
        mesn;
        mesq l("Ah, @@ is sooo amazing!", $MOST_HEROIC$);
        next;
    }
    mesn;
    mesq l("Have you ever heard of the [@@http://landoffire.org/|Land Of Fire@@]? It is a really cool game which is being developed by Pyndragon and Pihro!");
    next;
    mesn;
    mesq l("They had a TMWA server, but the Monster King went hyperactive and... Well, it crashed here.");
    if (.@q == 0) goto L_Quest;
    if (.@q == 1) goto L_Report;
    if (.@q == 2) goto L_Report; // To accomodate Pyn's node. Not required.

L_Menu:
    mes "";
    menu
        l("How can I reach the Land Of Fire?"), L_Where,
        l("Tell me about the Land Of Fire."),  L_Fire,
        l("What are the Transcendence Gates?"),  L_Gates,
        l("Thanks for the help."), L_Close;

// TODO: Perhaps it is worth mentioning on JSaves Castle is a Mana Source and thus, magic&skills work better, and weapons work worse?
L_Where:
    mes "";
    mesn;
    mesq l("If you head west, you'll eventually reach the magical Kamelot Castle.");
    next;
    mesn;
    mesq l("Inside it, there'll be a Transcendence Gate.");
    next;
    mesn;
    mesq l("It is a really cool place. You must visit it someday!");
    next;
    goto L_Menu;

L_Fire:
    mes "";
    mesn;
    mesq l("The Land Of Fire Village was the result from the collapse of two worlds. It is a huge village.");
    next;
    mesq l("It have lots of lava caves, and some cool stuff, like the @@, can only be craft there.", getitemlink(SaviorArmor));
    next;
    mesq l("Unique monsters can be found there, and people say about staffs which shoot raw death and doom from it!");
    next;
    mesq l("It's self sufficient economically, and many fairies enjoy travelling there. In fact, rumors says the Fairy Kingdom is near it!");
    next;
    goto L_Menu;

L_Gates:
    mes "";
    mesn;
    mesq l("Ah, you'll find on the Land Of Fire Village four transcendence gates.");
    next;
    mesn;
    mesq l("People from here always look dumbfolded when they walk past them and find themselves somewhere else.");
    next;
    mesn;
    mesq l("There's usually not a lot of things to do past these gates, but some monsters which only lives on the Land of Fire can only be found there!");
    next;
    goto L_Menu;

L_Close:
    closedialog;
    npctalkonce l("Ah, @@ is so amazing!", $MOST_HEROIC$);
    close;

L_Quest:
    mesn;
    mesq l("Actually, could I ask you a favor?");
    next;
    mesn;
    mesq l("I want a %s so badly... Pyndragon, in the forge, the master blacksmith which makes powerful weapons for high levels, can give you one.", getitemlink(LOFCoin));
    next;
    mesn;
    mesq l("Just tell him that I want to reconsider, and that I want the souvenir he offered me when I moved to Hurnscald.");
    next;
    setq HurnscaldQuest_LOFPass, 1;
    goto L_Menu;

L_Report:
    mes l("Did you brought me a %s?", getitemlink(LOFCoin));
    next;
    if (askyesno() == ASK_YES) goto L_Check;
    goto L_Menu;

L_Check:
    if (!countitem(LOFCoin)) goto L_NoItem;
    inventoryplace MoubooFigurine, 1;
    delitem LOFCoin, 1;
    getitem MoubooFigurine, 1;
    getexp 400, 20;
    setq HurnscaldQuest_LOFPass, 3;
    mesn;
    mesq lg("Thanks, my friend.");
    next;
    mesn;
    mesq l("Here, you can have this souvenir.");
    mesc l("* Item obtained: %s", getitemlink(MoubooFigurine));
    next;
    goto L_Menu;

L_NoItem:
    mesn;
    mesq l("I want a %s so badly... Pyndragon, in the forge, the master blacksmith which makes powerful weapons for high levels, can give you one.", getitemlink(LOFCoin));
    next;
    mesn;
    mesq l("Just tell him that I want to reconsider, and that I want the souvenir he offered me when I moved to Hurnscald.");
    next;
    goto L_Menu;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, CenturionHelmet);
    setunitdata(.@npcId, UDT_HEADMIDDLE, SaviorArmor);
    setunitdata(.@npcId, UDT_HEADBOTTOM, TulimsharGuardBoots);
    setunitdata(.@npcId, UDT_WEAPON, BromenalPants);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 1);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 1);
    npcsit;

    .sex = G_OTHER;
    .distance = 5;
    end;
}

