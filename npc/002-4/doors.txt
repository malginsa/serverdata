// TMW2 Scripts.
// Author:
//    Jesusalva
// Description:
//    Doors NPCs.

002-4,19,27,0	script	AreaNPC#002-4d	NPC_HIDDEN,0,0,{

OnTouch:
    if (LOCATION$ == "")
        warp "002-3", 43, 28;
    else
        warp "002-3@"+LOCATION$, 43, 28;
    close;
}
