// TMW2 scripts.
// Author:
//    gumi
//    Reid
//    Saulc
//    Jesusalva
// Description:
//    Water animations, splash, fishes, etc...

017-1,109,223,0	script	#Lof_WAM0	NPC_WATER_SPLASH,{

    fishing; // begin or continue fishing
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 4;
    end;
}

017-1,97,133,0	duplicate(#Lof_WAM0)	#Lof_WAM1	NPC_WATER_SPLASH
017-1,88,130,0	duplicate(#Lof_WAM0)	#Lof_WAM2	NPC_WATER_SPLASH
017-1,81,126,0	duplicate(#Lof_WAM0)	#Lof_WAM3	NPC_WATER_SPLASH

