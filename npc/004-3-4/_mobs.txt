// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 004-3-4: Canyon Cave mobs
004-3-4,59,29,21,10	monster	Bat	1039,4,35000,150000
004-3-4,60,29,31,10	monster	Cave Snake	1035,2,35000,150000
004-3-4,76,28,13,11	monster	Black Scorpion	1074,1,35000,150000
004-3-4,36,34,8,6	monster	Angry Scorpion	1131,1,35000,150000
004-3-4,0,0,0,0	monster	Cave Maggot	1027,1,35000,150000
004-3-4,0,0,0,0	monster	Plushroom Field	1011,1,35000,150000
