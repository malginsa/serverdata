// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Controls Eternal Swamps.
//    Contains functions to flood the map (hide the bridges - mask 1024 + mask 2)
//    Flood happens at random and causes BluePar to be spawn. Runs every 6/4 minutes.
//    At the 6th minute of an hour (6, 16, 26, 36, 46, 56)
//    At the 2nd minute of an hour (2, 14 ... 44, 54)

011-3,0,0,0	script	#EternalSwampCore	NPC_HIDDEN,{
    end;

OnSwampMob:
    end;

OnInit:
    addmapmask "011-3", MASK_SPECIAL;
    bindatcmd "es_flood", "#EternalSwampCore::OnSkipFlood", 99, 99, 1;
    end;

OnMinute02:
OnMinute06:
OnMinute12:
OnMinute16:
OnMinute22:
OnMinute26:
OnMinute32:
OnMinute36:
OnMinute42:
OnMinute46:
OnMinute52:
OnMinute56:
    // Check if auto-restart was scheduled
    SchedRestart();
OnSkipFlood:
    if ($@GM_OVERRIDE) debugmes "[Flood] Cycle begin";
    .@fd=!(getmapmask("011-3")&MASK_SPECIAL); // .@fd - is flooded?

    if (.@fd) {
        if ($@GM_OVERRIDE) debugmes "[Flood] UF - Unflooding";
        // If it is flooded, unflood it to prevent players getting struck for too long
        killmonster("011-3", "#EternalSwampCore::OnSwampMob");
        addmapmask "011-3", MASK_SPECIAL;
        //debugmes "[Flood] UF - Del Cells";
        delcells "ESwpBridge1";
        delcells "ESwpBridge2";
        delcells "ESwpBridge3";
        delcells "ESwpBridge4";
        delcells "ESwpBridge5";
        delcells "ESwpBridge6";
        delcells "ESwpBridge7";
        mapannounce "011-3", "Eternal Swamps: The flood ceases!",bc_all|bc_npc;
    } else {
        // 40% chances to flood, 75% during night
        .@odds=40;
        if (is_night())
            .@odds+=35;

        /*
        // Bugfix
        if ($@GM_OVERRIDE)
            .@odds=100;
        else
            .@odds=0;
        debugmes "[Flood] Analysis with %d odds to flood", .@odds;
        */

        // Maybe we should flood it
        if (rand(0,100) < .@odds) {
            if ($@GM_OVERRIDE) debugmes "[Flood] F - Flooding";
            removemapmask "011-3", MASK_SPECIAL;

            //debugmes "[Flood] F - Adding Cells";
            setcells "011-3", 37, 22, 39, 24, 1, "ESwpBridge1";
            setcells "011-3", 37, 34, 39, 41, 1, "ESwpBridge2";
            setcells "011-3", 31, 67, 33, 73, 1, "ESwpBridge3";
            setcells "011-3", 40, 96, 42, 102, 1, "ESwpBridge4";
            setcells "011-3", 38, 130, 40, 136, 1, "ESwpBridge5";
            setcells "011-3", 41, 157, 43, 163, 1, "ESwpBridge6";
            setcells "011-3", 36, 187, 38, 193, 1, "ESwpBridge7";

            //debugmes "[Flood] F - Spawn and Announce";
            areamonster "011-3", 20, 20, 60, 220, "Bluepar", Bluepar, rand(8,26), "#EternalSwampCore::OnSwampMob";
            mapannounce "011-3", "Eternal Swamps: A flood starts!",bc_all|bc_npc;

            // TODO: Handle players in bridges
            //debugmes "[Flood] F - Map Timer OK";
            maptimer("011-3", 100, "#EternalSwampCore::OnBridgeDown");
        }

    }
    if ($@GM_OVERRIDE) debugmes "[Flood] Cycle finished";
    end;

// Fix players struck by setcells
OnBridgeDown:
    //debugmes "[Flood] [OnBD] Bridge is Down";
    if (isin("011-3", 37, 22, 39, 24))
        slide 38, 21;
    else if (isin("011-3", 37, 34, 39, 41))
        slide 38, 33;
    else if (isin("011-3", 31, 67, 33, 73))
        slide 32, 65;
    else if (isin("011-3", 40, 96, 42, 102))
        slide 41, 95;
    else if (isin("011-3", 38, 130, 40, 136))
        slide 39, 128;
    else if (isin("011-3", 41, 157, 43, 163))
        slide 42, 155;
    else if (isin("011-3", 36, 187, 38, 193))
        slide 37, 185;
    //debugmes "[Flood] [OnBD] Finished";

    end;
}

