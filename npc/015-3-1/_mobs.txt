// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 015-3-1: Pirate Caves B1F mobs
015-3-1,43,72,3,4	monster	Thug	1442,2,100000,30000
015-3-1,41,73,3,4	monster	Swashbuckler	1443,1,100000,30000
015-3-1,46,74,3,4	monster	Grenadier	1444,1,100000,30000
015-3-1,28,69,4,1	monster	Thug	1442,2,100000,30000
015-3-1,71,63,3,4	monster	Thug	1442,2,100000,30000
015-3-1,39,39,3,1	monster	Swashbuckler	1443,2,100000,30000
015-3-1,71,63,3,4	monster	Grenadier	1444,1,100000,30000
015-3-1,64,49,6,4	monster	Swashbuckler	1443,1,100000,30000
015-3-1,29,92,6,4	monster	Swashbuckler	1443,1,100000,30000
015-3-1,33,50,8,2	monster	Thug	1442,2,100000,30000
