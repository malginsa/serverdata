// TMW2 Script.
// Author:
//    Jesusalva
// Description:
//    Random Sign nobody bothers reading.
//    I just got lazy. Maybe I'll sell these two houses for 1000000 GP each... :drolls:

019-2,95,108,0	script	Sign#019295108	NPC_NO_SPRITE,{
    if (!$NIVALIS_LIBDATE) {
        mesc l("WARNING: Nivalis is currently under siege from the Monster King himself."), 3;
        next;
        mesc l("The Alliance Advanced Outposts are closed for maintenance. Group in front of the town entrance!");
    } else {
        mesc l("Welcome to Nivalis, the frozen town.");
        mesc l("The Alliance Advanced Outposts have been abandoned and locked after the Liberation day.");
    }
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 3;
    end;
}
