// TMW2 scripts.
// Authors:
//    Jesusalva
// Originals:        Ernando <ernando.quirino@hotmail.com>
// Original review:  Lunovox <rui.gravata@gmail.com>; Ernando <ernando.quirino@hotmail.com>; Jesusalva <supremo@brasil.byethost22.com>
// Objective:        Train the player and give some experience.
// License:          GPL v3
//
// CandorQuest_Trainer
//  0:  Not started

//  1:  Took Maggot Quest
//  2:  Completed Maggot Quest
//  3:  Awaiting next quest

//  4:  Took House Maggot Quest
//  5:  Completed House Maggot Quest
//  6:  Awaiting next quest

//  7:  Took Candor Scorpion Quest
//  8:  Completed Candor Scorpion Quest
//  9:  Awaiting next quest

// 10:  Took Scorpion Quest
// 11:  Completed Scorpion Quest
// 12:  Finished all training

005-7,25,29,0	script	Trainer	NPC_PLAYER,{
    goto L_Begin;

OnRemoteHelp:
L_Begin:
    .@q=getq(CandorQuest_Trainer);
    .@b=getq(ShipQuests_Knife); // TODO: We should check if user is with weapon equipped instead
    .@map$=getmap();
    mesn;
    mesq lg("Hello my friend! Need my help?");
    next;
    goto L_Menu;

L_PreMenu:
    mesn;
    mesq l("Do you need my help?");
    next;
    goto L_Menu;

L_Menu:
    mesn strcharinfo(0);
    menu
        l("Why do you want to help me?"),       L_Backstory,
        rif(.@map$ == "005-7" && getq(CandorQuest_Trainer) < 15, l("I want to be trained!")), L_Training,
        l("I wanted info about how to play."),  L_Manaplus_gap,
        l("How do I make money?"),              L_MakingMoney,
        l("Monsters."),                         L_Monsters,
        l("Quests."),                           L_Quests,
        l("Status."),                           L_Status,
        l("NPCs."),                             L_NPC,
        l("Magic."),                            L_Magic,
        l("Rules."),                            L_Rules,
        l("Weather & Seasons."),                L_Seasons,
        l("Experience."),                       L_Experience,
        l("No, thanks!"),                       L_Close;


L_Manaplus_gap:
    mes "";
    goto L_Manaplus;

L_Manaplus:
    mesn;
    mesq l("What do you want to learn more about?");
    next;
    mesn strcharinfo(0);
    menu
        l("How do I see my items?"),             L_ArmorItems,
        l("How do I trade with other players?"), L_Trading,
        l("How do I hunt monsters?"),            L_HuntingMonsters,
        l("How do I talk with someone?"),        L_Talking,
        l("Hairstyles."),                        L_Hairstyle,
        l("Commands."),                          L_Commands,
        l("I changed my mind."),                 L_Menu_gap;

L_Training:
    .@q=getq(CandorQuest_Trainer);
    mes "";
    if (BaseLevel < 3) goto L_NoLevel;
    if (.@b == 0) goto L_NoKnife;
    .@k=getq2(CandorQuest_Trainer); // Get number of kills (via getq2)

    mesn;
    if (.@q == 0) {
        mesq l("It looks like you are ready. Let me see if there is an easy task for you...");
        next;
        mesq l("Ah! I know. Kill 10 @@. They are usually found in the fields, feeding on the crops.", getmonsterlink(Maggot));
        setq CandorQuest_Trainer, 1, 0;
    } else if (.@q == 1) {
        mesq l("You are currently tasked with killing @@. As a reminder, you can usually find them where we grow our crops.", getmonsterlink(Maggot));
    } else if (.@q == 2) {
        mesq l("Good job! Here is your reward!");
        getexp 30, 0;
        set Zeny, Zeny + 25;
        setq CandorQuest_Trainer, 3, 0;
    } else if (.@q == 3) {
        mesq l("Less maggots means a more reliable food supply. This kind of maggot will be a little more difficult to defeat, which is an excellent way to test your prowess in battle.");
        next;
        mesq l("In the storehouse, kill 4 @@. They keep coming back and contaminate our stored goods.", getmonsterlink(HouseMaggot));
        next;
        mesq l("Our dear mayoress may also need help dealing with that maggot infestation.");
        setq CandorQuest_Trainer, 4, 0;
    } else if (.@q == 4) {
        mesq l("Kill the @@ at the storehouse.", getmonsterlink(HouseMaggot));
    } else if (.@q == 5) {
        mesq l("Perfect! Here is your reward!");
        getexp 42, 0;
        set Zeny, Zeny + 25;
        setq CandorQuest_Trainer, 6, 0;
    } else if (.@q == 6) {
        mesq l("As you learned, helping others is a good way to level up. You can also sell monster parts for some money.");
        next;
        mesq l("So, please kill 3 @@, which are usually found around our beaches. You can sell their stingers if you are short on money.", getmonsterlink(CandorScorpion));
        setq CandorQuest_Trainer, 7, 0;
    } else if (.@q == 7) {
        mesq l("The @@ I told you to kill are usually at the beach.", getmonsterlink(CandorScorpion));
    } else if (.@q == 8) {
        inventoryplace SmallKnife, 1;
        mesq l("Managed to get any nice drops? It requires more luck than skill. Here is your reward.");
        getexp 53, 0;
        set Zeny, Zeny + 25;
        getitem SmallKnife, 1;
        setq CandorQuest_Trainer, 9, 0;
    } else if (.@q == 9) {
        mesq l("This will be your last task. Besides the @@, the strongest monster on this island worth killing is the @@.", getmonsterlink(ManaBug), getmonsterlink(Scorpion));
        next;
        mesq l("Kill 2 @@. This will prove your worth. They are northwest of the island.", getmonsterlink(Scorpion));
        setq CandorQuest_Trainer, 10, 0;
    } else if (.@q == 10) {
        mesq l("I want you to kill @@, which are located near the northwest coast of the island.", getmonsterlink(Scorpion));
    } else if (.@q == 11) {
        mesq l("Congratulations! Here is your reward.");
        mesq l("You completed your training, so you're getting 50 bonus experience points. If you level up, use your stat points wisely!");
        getexp 70, 0;
        set Zeny, Zeny + 25;
        setq CandorQuest_Trainer, 12, 0;
    } else if (.@q == 12 && BaseLevel >= 9) {
        mesq l("You are already fully trained. You should go to Tulimshar, but if you want a bonus task, please kill a @@.", getmonsterlink(ManaBug));
        mesq l("It should be almost a boss at your level, so I don't expect to see you again here so soon.");
        setq CandorQuest_Trainer, 13, 0;
    } else if (.@q == 12 && BaseLevel < 9) {
        mesq l("You are already fully trained. You should go to Tulimshar. I may have another task for you later, but you are too weak now, get some levels.");
    } else if (.@q == 13) {
        mesq l("You are trying to kill a @@.", getmonsterlink(ManaBug));
        mesq l("It should be almost a boss at your level, so I don't expect to see you again here so soon.");
    } else if (.@q == 14) {
        mesq l("Wow! You did it! I do not think anyone else could have done that.");
        mesq l("Here, take this @@ - you deserve it! And here is 200 GP to buy a better weapon.", getitemlink(CandorHeadBand));
        inventoryplace CandorHeadBand, 1;
        getexp 80, 5;
        getitem CandorHeadBand, 1;
        set Zeny, Zeny + 200;
        setq CandorQuest_Trainer, 15, 0;
    }
    next;
    goto L_PreMenu;

L_NoLevel:
    mesn;
    mesq l("Ah, yes... You see, there is just no task I can give to you right now. ##BYou are too weak to fight monsters.##b");
    next;
    mesn;
    mesq l("Try doing quests which doesn't involve monster hunting first. I'm sure ##B Ayasha ##b could use your help.");
    next;
    mesn;
    mesq l("Otherwise, the monsters here usually won't attack you unless provoked.");
    next;
    mesn;
    mesq l("Complete quests, gain some experience, allocate some status, and you'll be ready for my training.");
    next;
    goto L_PreMenu;

L_NoKnife:
    mesn;
    mesq l("And with what you expect to fight monsters? You must arrange yourself a weapon!");
    next;
    mesn;
    mesq l("I don't know. Try getting a Rusty Knife or something. Maybe the chef of Nard's ship can spare you one.");
    next;
    mesn;
    mesq l("There probably is a huge, flashing orange exclamation mark over a suitable knife you could take and nobody would mind.");
    next;
    goto L_PreMenu;

// Anything below this line is copy-pasted from SG:ManaBird, a TMW-BR clone
// It was translated by Google and therefore may have bad terms about ManaPlus interface.
L_Backstory:
    mes "";
    mesn;
    mesq l("Saxso, the former mayor, commanded me to strengthen the youngsters, so that they have sufficient power to fight monsters.");
    next;
    mesn;
    mesq lg("He died, but I plan in fulfilling his will. I can give you training for that, and teach you how to fight properly.");
    next;
    mesn;
    mesq l("I see you have arms long enough to be an archer.");
    next;
    goto L_PreMenu;

L_MakingMoney:
    mes "";
    mesn;
    mes l("Merchants like to buy body parts of killed monsters and animals because they can make items and equipment.");
    next;
    mesn;
    mes l("Some others also like to buy them to keep as trophies. Either way, you can make some money with that.");
    next;
    mesn;
    mes l("You must find someone willing to buy, they usually will buy almost anything you have, even items which cannot be replaced, so be careful.");
    next;
    mesn;
    mes l("You must \"add\" the items you plan on selling, and then press \"sell\" to confirm. You'll have this time to review.");
    mes l("Some, but not all, from the rare or non-replaceable items will have a warning when you try to sell them.");
    next;
    mesn;
    mes l("You can also make money ##Bdoing quests##b. Elmo will tell you almost every quest which can be done in Candor.");
    next;
    goto L_PreMenu;

L_ArmorItems:
    mes "";
    mesn;
    mes l("You can see all your equipment by pressing the F3 key.");
    next;
    mesn;
    mes l("To equip or unequip an item, select it and press the 'Equip' or 'Unequip' button. You can not 'Equip' or 'Unequip' when talking to someone.");
    next;
    mesn;
    mes l("Dress up! Do not walk without clothes! Always wear your items! They leave you less vulnerable to attacks and stronger to defeat your opponents.");
    next;
    mesn;
    mes "##B"+l("Remember that some equipment sets will give you hidden stat bonuses! So dress yourself in a fashion way, if possible!")+"##b";
    next;
    mesn;
    mes l("To discard an item you no longer want, select it and press the 'Discard' button. Generic items can be discarded or sold.");
    mes l("Some special items cannot be traded, discarded, nor sold. With a right click, you can also protect normal items as if they were special ones.");
    next;
    mesn;
    mes l("There are three types of items.");
    mes l("Those for consumption, equipment and generics.");
    next;
    mesn;
    mes l("Items for consumption, like potions, can only be used once.");
    mes l("Once used, they will disappear from your inventory.");
    next;
    mesn;
    mes l("Equippable items are armour, weapons and accessories.");
    mes l("They can be equipped to make your look more interesting or to improve some of its features.");
    next;
    mesn;
    mesq l("Generic items are used for different purposes. In creating other items, to swap and sell, to collect, etc.");
    next;
    goto L_Manaplus;

L_Trading:
    mes "";
    mesn;
    mes l("Press the 'R' key to ignore or accept business proposals. You and the other citizen who want to negotiate need to be in the configuration that accepts negotiations. if your configuration is 'Ignoring business proposals', then you will not receive the warning from any citizen wanting to negotiate with you, and you will not be able to initiate negotiations.");
    next;
    mesn;
    mes l("To negotiate with other citizens, you should click the second mouse button on some other citizen who is accepting negotiations, and select the 'Negotiation' option from the menu that will appear.");
    next;
    mesn;
    mes l("After you have confirmed the negotiation, a window with a vertical split will appear. The left side are the items you intend to offer in trading. The right side are the items that the other citizen intends to offer in trading.");
    next;
    mesn;
    mes l("Open your inventory window (F3 key) next to the trading window. Select an item you want to offer, and then press the Add button. To add money to the negotiation, enter the amount you will offer and press the Change button.");
    next;
    mesn;
    mes l("When you have added all the items and money you want, press the 'Propose Business' button. The other citizen must also press the 'Propose Business' button.");
    next;
    mesn;
    mesq l("if the proposal is not convenient for you, just close the trading window to cancel the exchange of items and money. But if both press the 'Accept Negotiation' button, then the marketing will be finished.");
    next;
    mesn;
    mes l("Remember! You're trading things, not lending/borrowing them. You are solely responsible for everything you own.");
    next;
    goto L_Manaplus;

// TODO: We have over nine instructions here. You usually can only memorise from three to five at a time!
L_HuntingMonsters:
    mes "";
    mesn;
    mesq l("Note down. To hunt a target you must click the primary mouse button on it. Avoid fighting monsters or citizens much stronger than you. ##BYou will lose experience if you are defeated.##b");
    next;
    mesn;
    mes l("Within the cities is a place safe enough not to be attacked by another person (except during wars). But outside of them there are some places where the citizen can be attacked by enemies from other realms, or even by someone from the same realm.");
    next;
    mesn;
    mes l("There are some stones scattered around the world that mark your point of return in case of defeats. Some ship chests may also serve as a return point. You can also select some beds in case of defeats.");
    next;
    mesn;
    mes l("Almost all creatures drop useful items when defeated. To get the dropped item press the 'Z' key next to the item or click the primary button on the item.");
    next;
    mesn;
    mes l("To focus on a creature, press the 'A' key. To focus on another citizen, press the 'Q' key. To attack the focused target press the 'X' key or click the primary button on the creature.");
    next;
    mesn;
    mes l("To focus on an NPC, press the 'N' key. To talk to him press the 'T' key.");
    next;
    mesn;
    mes l("To defocus or stop attacking, press Shift + A.");
    next;
    mesn;
    mes l("You can, however, use ##BCtrl##b to auto-select a monster and attack them. This usually also collects drops, but press Z to be sure.");
    next;
    goto L_Manaplus;

L_Talking:
    mes "";
    mesn;
    mes l("To display the dialog box with other citizens, press the F7 key.");
    next;
    mesn;
    mes l("To speak in public select the 'General' tab. It serves to talk to people who are appearing on your screen.");
    next;
    mesn;
    mes l("To speak privately with someone, click the second mouse button on the citizen and select the 'Whisper' option.");
    next;
    mesn;
    mes l("In order to enter a message press the 'Enter' key, this will display the white box of typing. Type your message there and press 'Enter' again to send your speech.");
    next;
    mesn;
    mes l("To speak privately to a friend who is not appearing on your screen, type the command '##B /q Citizen Name ##b' and press 'Enter'. This command will open a long-distance dialog that has the name of who you want to talk to. Select this new tab and send your message through it.");
    next;
    mesn;
    mes l("And by last, to speak to everyone online, besides whoever might be idling on Discord, select the '#world' tab.");
    next;
    mesn;
    mes l("But be careful: do not scream when using a lot of capital letters, and do not keep repeating the lines, and above all DO NOT SPAM, or you may be severely penalized.");
    next;
    goto L_Manaplus;

L_Close:
    close;

L_Monsters:
    mes "";
    mesn;
    mesq l("Monsters are everywhere. They're a plague we're trying to get rid of.");
    next;
    mesn;
    mesq l("There are three types of monsters: the aggressive, the neutral, and the collaborative.");
    next;
    mesn;
    mes l("Aggressors always know when they are in danger! Therefore, they are always on standby, attacking anyone who appears ahead.");
    next;
    mesn;
    mes l("Neutral monsters do not have such a sense of danger.");
    mes l("They will not attack anyone unless they are attacked first.");
    next;
    mesn;
    mes l("Normally, collaborative behave like neutral monsters. Unless some partner of the same species is in danger, at which point they all take an aggressive stance against the aggressor.");
    mes l("It's always good to see if you have a lot of them around before you think about attacking one!");
    next;
    mesn;
    mes "\""+l("Also, most monsters get enraged and will attack whoever is closest to them, regardless of anything else.");
    mes l("Not all monsters will do this, but most will. So if you see a monster running after a player and you stand in the way...");
    mes l("...It'll most likely attack you, instead.")+"\"";
    next;
    mesn;
    mes l("One last thing to keep in mind... If you are surrounded, you'll suffer an agility and defense penalty.");
    mes l("But if you and other players surrounds the monster instead, they'll suffer the same penalties!");
    mes l("Any boss which was previously unhittable, can be hit with appropriate number of attackers.");
    next;
    goto L_PreMenu;

L_Hairstyle:
    mes "";
    mesn;
    mes l("NPC stylists can cut your hair and give you a new style, which means the hair style you woke up is something that can be changed.");
    mes l("They are known to use a revolutionary hair growth formula, which can give you a wild thatch even if you are bald!");
    next;
    goto L_Manaplus;

L_Quests:
    mes "";
    mesn;
    mes l("There are people in the world who need help!");
    mes l("Most of these people will not think twice before giving a nice reward to anyone who helps them.");
    mes l("So be nice and help people along the way!");
    next;
    mesn;
    mesq l("Seldomly, they'll have an exclamation mark over their heads. But some quests are hidden, so talk to people and have fun!");
    next;
    goto L_PreMenu;

L_NPC:
    mes "";
    mesn;
    mes l("NPCs(Non Playable Characters) or non-playable characters are characters that are always in the game, offering a wide variety of reactions, from a simple friendly conversation to a desperate request for help.");
    next;
    mes l("##BIMPORTANT:##b People usually doesn't shout, they talk. Because this, if you are too far, an NPC won't hear you.");
    mes l("When this is the case, you should get closer to the NPC, until they hear you.");
    mes l("If you are above the NPC and they still doesn't hear you, this mean they are deaf - you should report this!");
    goto L_PreMenu;

L_Commands:
    mes "";
    mesn;
    mes l("/ clear clears the text box.");
    mes l("/ whisper [name] allows you to send a private message to the player. if [name] contains spaces, it must be enclosed in quotation marks.");
    //mes l("/who mostra o número de jogadores conectados no momento.");
    mes l("/ present shows the number of people in the neighbourhood.");
    mes l("/ where shows the name of the map you are in.");
    mes l("/ help explains how to use all client commands.");
    mes l("@commands lists even more advanced commands, but you can't use all of them.");
    next;
    mes l("@resync will help when the client starts lagging. If you see an attack but no monsters, that's the cause.");
    mes l("@lang allows you to change game language, anytime, anywhere.");
    mes l("@rules will tell you all the rules once again.");
    mes l("@toevent will warp you to event island, if an event is happening, of course.");
    mes l("@discord allows you to setup Discord integration settings.");
    mes l("@ucp allows you to manage your account, eg. recover lost email.");
    next;
    mes l("@resyncall is the more powerful version of @resync. It'll reload everything, even the clouds if needed.");
    mes l("@info and @tutorial will, using Jesusalva's powers, allow you to contact me anywhere for info.");
    mes l("/ mi does the same as @monsterinfo. Takes the monster name as argument and reports monster stats and drops.");
    next;
    goto L_Manaplus;

L_Status:
    mes "";
    mesn;
    mesq l("People vary greatly in the amount of strength, agility, dexterity, intelligence, vitality and luck.");
    next;
    mesn;
    mes l("@@ helps you carry more items and also gives you a more forceful blow, but ends up not being very interesting if you focus on weapons that use projectiles, such as the bow.", b(l("Strength")));
    mes l("Greater @@ allows you to attack faster and has a greater chance of evading attacks.", b(l("agility")));
    mes l("Your @@ determines your ability to hit monsters and is valuable to players who prefer weapons that use projectiles.", b(l("dexterity")));
    next;
    mesn;
    mes l("@@ determines how many blows you can take before you die. It also affects status effects, like poison.", b(l("Vitality")));
    mes l("@@ is very useful for alchemy and magic, but nowadays there are few opportunities to use it.", b(l("Intelligence")));
    mes l("Your @@ determines several small things, including critical attacks and, limited to a certain extent, affect drop rates.", b(l("luck")));
    next;
    mesn;
    mes l("A critical hit deals added damage and disregards defense. A critical always hit, although it can be blocked just fine.");
    mes l("On a side note, more defense is always good, but the damage won't decrease on the same rate that defense raises.");
    mes l("Also note that if you are in overweight, your natural regen will halt. 90% in weight, and you won't be able to attack.");
    next;
    mesn;
    mes l("I recommend that you train your agility a great deal, since most monsters out there aren't really amazing at hitting you.");
    mes l("For now do not take too much time to work on your intelligence, after all, almost nobody have magic this day.");
    next;
    mesn;
    mes l("You can allocate point on those attributes every time you level up.");
    mes l("There's also a job level, which produces green sparkles when you level it.");
    mes l("Job Level and certain equips can affect your status. You'll see the modifiers with a + sign.");
    next;
    goto L_PreMenu;

L_Magic:
    mes "";
    mesn;
    mesq l("Magic is dead. Well, not yet, we still have some mana stones left - but only the strongest ones are allowed to use them and acquire magic.");
    next;
    mesn;
    mesq l("I've heard about some adventurers whom obtained magic in another way, but you would still need a lot of levels for that.");
    next;
    goto L_PreMenu;

L_Seasons:
    mes "";
    mesn;
    mes l("First of, there's a day/night cycle on the game.");
    if (is_night())
        mes l("It's currently night, that's why Candor is dark.");
    else
        mes l("It's currently day, but when night falls, Candor will become darker.");
    next;
    mesn;
    mes l("During night, the monsters usually respawn faster. That can be a problem with aggressive monsters.");
    mes l("I also hear fisherman likes to fish at night. They say the catch is bigger, if you understand me.");
    next;
    mesn;
    mes l("Not only that, but at night monsters are stronger! They also give more experience and drop more often to compensate, though.");
    next;
    mesn;
    mes l("There's also weather, meaning it can rain, snow, or even happen a sandstorm. They are usually cosmetic, but...");
    mes l("...who knows if there isn't a secret in that?");
    next;
    mesn;
    mes l("Besides this, there is Seasons. You know, summer, autumn, winter and spring.");
    mes l("Each season unlocks a set of quests and drops which can only be obtained on the season.");
    next;
    mesn;
    mes l("Think on Season Quests as a yearly quest which you have three months to do.");
    mes l("We follow north hemisphere seasons in case you're wondering.");
    next;
    goto L_PreMenu;

L_Experience:
    mes "";
    mesn;
    mes l("Experience can be gained by completing quests and killing monsters. When you accumulate enough experience, you'll level up!");
    next;
    mesn;
    mes l("Each level up will buff your base stats, and give you stats points to allocate. However, there is Job Level.");
    next;
    mesn;
    mes l("There's also a job level, which produces green sparkles when you level it.");
    mes l("Job Level and certain equips can affect your status. You'll see the modifiers with a + sign.");
    next;
    mesn;
    mes l("Also, you'll get more experience by killing monsters stronger than you, and less experience by killing monsters weaker than you.");
    mes l("You can find out the monster strength by using \"@monsterinfo <English Monster Name>\". Check the level in it!");
    next;
    goto L_PreMenu;

L_Rules:
    mes "";
    callfunc "GameRules";
    next;
    mes ".:: "+l("Automatic Botchecks")+" ::.";
    mes l("If enabled, automatic captchas may be sent to you every once in a while.");
    mes l("To answer them, use: @captcha <num_value>");
    next;
    CaptchExample();
    mes l("Example: Give the answer for the following: one+1");
    mes l("Reply: %s", b("@captcha 2"));
    next;
    goto L_PreMenu;

L_Menu_gap:
    mes "";
    goto L_PreMenu;

    function trainer_add_kills
    {
        .@qp=getq(CandorQuest_Trainer);
        .@kp=getq2(CandorQuest_Trainer); // Get number of kills (via getq2)
        setq CandorQuest_Trainer, .@qp, .@kp+1;
        //message strcharinfo(0), l("Set status @@ with @@ kills", .@qp, .@kp);
        return;
    }

    function trainer_max_kills
    {
        .@qp=getq(CandorQuest_Trainer);
        setq CandorQuest_Trainer, .@qp+1, 0;
        //message strcharinfo(0), l("End status @@", .@qp);
        return;
    }

OnKillMaggot:
    .@q=getq(CandorQuest_Trainer);
    .@k=getq2(CandorQuest_Trainer); // Get number of kills (via getq2)
    if (.@q == 1) {
        if (.@k+1 >= 10) {
            trainer_max_kills();
            message strcharinfo(0), l("All maggots are dead!");
        } else {
            trainer_add_kills();
            message strcharinfo(0), l("@@/10 Maggots", .@k+1);
        }
    }
    // Additional Cactus Drink drop rate for newcomers: 4.9% additional
    if (BaseLevel <= 22) {
        if (rand2(10000) < 490-(BaseLevel**2)) {
            getmapxy(.@m$, .@x, .@y, 0);
            makeitem CactusDrink, 1, .@m$, .@x, .@y;
        }
    }
    fix_mobkill(Maggot);
    end;
OnKillHouseMaggot:
    .@q=getq(CandorQuest_Trainer);
    .@k=getq2(CandorQuest_Trainer); // Get number of kills (via getq2)
    if (.@q == 4) {
        if (.@k+1 >= 4) {
            trainer_max_kills();
            message strcharinfo(0), l("All house maggots are dead!");
        } else {
            trainer_add_kills();
            message strcharinfo(0), l("@@/4 House Maggots", .@k+1);
        }
    } else {
        if (is_staff())
            dispbottom "It's working. (T:OKHM)";
    }
    fix_mobkill(HouseMaggot);
    end;
OnKillCandorScorpion:
    if (!playerattached()) end;
    .@q=getq(CandorQuest_Trainer);
    .@k=getq2(CandorQuest_Trainer); // Get number of kills (via getq2)
    if (.@q == 7) {
        if (.@k+1 >= 3) {
            trainer_max_kills();
            message strcharinfo(0), l("All candor scorpions are dead!");
        } else {
            trainer_add_kills();
            message strcharinfo(0), l("@@/3 Candor Scorpions", .@k+1);
        }
    }
    // Additional Bug Leg drop rate for newcomers: 5% additional
    if (BaseLevel <= 22) {
        if (rand2(10000) < 500-(BaseLevel**2)) {
            getmapxy(.@m$, .@x, .@y, 0);
            makeitem BugLeg, 1, .@m$, .@x, .@y;
        }
    }
    fix_mobkill(CandorScorpion);
    end;
OnKillScorpion:
    .@q=getq(CandorQuest_Trainer);
    .@k=getq2(CandorQuest_Trainer); // Get number of kills (via getq2)
    if (.@q == 10) {
        if (.@k+1 >= 2) {
            trainer_max_kills();
            message strcharinfo(0), l("All scorpions are dead!");
        } else {
            trainer_add_kills();
            message strcharinfo(0), l("@@/2 Scorpion", .@k+1);
        }
    }
    fix_mobkill(Scorpion);
    end;
OnKillManaBug:
    .@q=getq(CandorQuest_Trainer);
    .@k=getq2(CandorQuest_Trainer); // Get number of kills (via getq2)
    if (.@q == 13) {
        if (.@k+1 >= 1) {
            trainer_max_kills();
            message strcharinfo(0), l("All mana bugs are dead!");
        } else {
            trainer_add_kills();
            message strcharinfo(0), l("@@/1 Mana Bug", .@k+1);
        }
    }
    fix_mobkill(ManaBug);
    end;

OnInit:
    bindatcmd "info", "Trainer::OnRemoteHelp", 0, 60, 0;
    bindatcmd "tutorial", "Trainer::OnRemoteHelp", 0, 60, 0;

    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, LeatherShirt);
    setunitdata(.@npcId, UDT_HEADMIDDLE, CottonTrousers);
    setunitdata(.@npcId, UDT_HEADBOTTOM, NPCEyes);
    setunitdata(.@npcId, UDT_WEAPON, DeepBlackBoots);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 20);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 7);

    .sex = G_MALE;
    .distance = 5;
    end;
}
