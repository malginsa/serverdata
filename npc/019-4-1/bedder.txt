// TMW2/LoF scripts.
// Authors:
//    TMW-LoF Team
//    Jesusalva
// Description:
//    Christmas Quest
//    In dire need for white fur for bedding. Gives %Reward% for %Amount% (LESS LIKELY)
//      (TMW Org. Version)
//    Or maybe, he just take the white fur + something + open xmas box and trades
//    these for closed gift boxes (MOST LIKELY)
//      TMW2:ML Version

019-4-1,48,39,0	script	Christmas Storage Master	NPC_GNOME_C,{
    if ($EVENT$ != "Christmas")
        goto L_OutOfSeason;
    mesn;
    mesq l("I was informed that our bedding material for fragile presents is nearly depleted...");
    next;
    goto L_Main;

L_OutOfSeason:
    mesn;
    mesq l("According to the Holy books of a parallel world, Christmas is an event to celebrate the birth of someone very important, the son of God, whom have the promised kingdom.");
    next;
    mesn;
    mesq l("Years later, someone then started giving gifts to the poor childrens on that date. It sticked, and now people usually trade gifts each other on the date.");
    next;
    mesn;
    mesq l("It is known that even world wars have been under cease-fire on this date. On our world, Jesus Saves pays fortunes to Santa so he can gift every adventurer for the good year.");
    next;
    mesn;
    mesq l("When Christmas starts, here will become frantic again, with things running out of stock all the time... But the adventurers deserve it, for fighting monsters the year round.");
    close;

L_Main:
    mesn;
    mesq l("You can give @@ to help us, I'll pay you accordingly.", getitemlink(WhiteFur)); // Normal: 30. Here: 75.
    mesq l("Or you can give me an @@, 3 @@ and an @@, and I'll make a gift for you. If you don't have the @@, you can pay 200 GP for it instead.", getitemlink(OpenedChristmasBox), getitemlink(WhiteFur), getitemlink(MoubooFigurine), getitemlink(MoubooFigurine));
    mes "";
    select
        l("Good to know."),
        rif(countitem(WhiteFur), l("I want to sell White Fur for 75 GP each")),
        rif(countitem(OpenedChristmasBox) && countitem(WhiteFur) >= 3 && countitem(MoubooFigurine), l("I want a gift box and here is the gift.")),
        rif(countitem(OpenedChristmasBox) && countitem(WhiteFur) >= 3 && Zeny >= 200, l("I want a gift box but I don't have a gift."));
    mes "";
    switch (@menu) {
        case 2:
            mesc l("How many do you want to sell? You currently have @@ @@.", countitem(WhiteFur), getitemlink(WhiteFur));
            input .@count;
            mes "";
            if (countitem(WhiteFur) < .@count) {
                mesc l("You don't have that.");
                next;
                goto L_Main;
            } else {
                delitem WhiteFur, .@count;
                Zeny=Zeny+75*(.@count);
                mesn;
                mesq l("Thanks. A pleasure doing business with you.");
                next;
            }
            break;
        case 3:
            inventoryplace ClosedChristmasBox, 1;
            delitem WhiteFur, 3;
            delitem MoubooFigurine, 1;
            delitem OpenedChristmasBox, 1;
            getitem ClosedChristmasBox, 1;
            mesn;
            mesq l("Here you go, anything else?");
            next;
            break;
        case 4:
            inventoryplace ClosedChristmasBox, 1;
            delitem WhiteFur, 3;
            Zeny=Zeny-200;
            $XMAS_MONEY+=100;
            delitem OpenedChristmasBox, 1;
            getitem ClosedChristmasBox, 1;
            mesn;
            mesq l("Here you go, anything else?");
            next;
            break;
        default:
            close;
    }

    goto L_Main;

OnInit:
    .sex=G_MALE;
    .distance=5;
    end;
}

