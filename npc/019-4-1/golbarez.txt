// TMW2/LoF scripts.
// Authors:
//    TMW-LoF Team
//    Jesusalva
// Description:
//    Christmas quest. ported from 2010 and adapted for TMW2:ML.
//    Original Name: Golbenez
//    Year, Gifts, GP

019-4-1,30,45,0	script	Golbarez	NPC_LOF_RICH,{
    if ($EVENT$ != "Christmas")
        goto L_OutOfSeason;
    goto L_Main;

L_OutOfSeason:
    mesn;
    if (any(true, false))
        mesq l("AAH! You scared me!");
    else
        mesq l("Land Of Fire is a place so warm... Although this workshop is a better place to break time and space during Christmas.");
    close;

L_Main:
    if (getq(SQuest_Christmas) != gettime(GETTIME_YEAR) &&
        !(gettime(GETTIME_MONTH) == JANUARY && getq(SQuest_Christmas) == gettime(GETTIME_YEAR)-1)) {
        mesn;
        mesq l("Please talk with this workshop's Chief before talking to me.");
        close;
    }
    .@q=getq3(SQuest_Christmas);
    .@ratio=(.@q*100)/$XMAS_MONEY;
    mesn;
    mesq l("Mortal! I am @@! I have broken through the barriers of space and time!", .name$);
    mesq l("I want to keep breaking them, until I find the paradise! Thus far, I only found the Land Of Fire!");
    next;
    mesn;
    mesq l("I need millions of GP to fund my time-space shattering, and thus far, I've only collected @@ GP.", format_number($XMAS_MONEY));
    mesq l("If you sponsor me, while I break into more dimensions, I'll give you any stuff I find. Hey, they could be rare here!");
    next;
    mesc l("Sponsor @@ in how much GP?", .name$);
    input .@count;
    mes "";
    if (Zeny < .@count) {
        mesn;
        mesq l("Don't try to trick me, this attracts bad karma! You could get stolen on Christmas! Seriously, this has happened before!");
        close;
    }
    Zeny=Zeny-.@count;
    $XMAS_MONEY+=.@count;
    setq3 SQuest_Christmas, .@q+.@count;
    mesn;
    mesq l("Thanks for your patronage! The rewards will be available with @@, in case I do find the paradise!", "Christmas Chief");
    mesc l("Thus far, you've sponsored Golbarez in %d %% from total.", .@ratio);
    mesc l("Note: No rares will be given if Golbarez doesn't get enough GP to find the paradise, but Strange Coins will still be given.");
    close;

OnInit:
    .sex=G_MALE;
    .distance=5;
    end;
}

