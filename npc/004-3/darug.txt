// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Part of Ancient Temple Quest

004-3,81,22,0	script	Darug	NPC_INDIGENOUS_CHIEF,{
    mesn;
    mesq l("Greetings, traveler. I am Darug, chief of this clan.");
    next;
    mesn;
    mesq l("Since the war outbreak, we have to keep moving constantly. Without strong walls to protect ourselves, though, our clan is on the verge of extinction.");
    next;
    mesn;
    mesq l("We know a lot, but all this knowledge will part with us. We should not the only nomads, though. There should be nomads in Argaes, Kaizei, besides the citadels of Oceania and Volcania. Unfortunately, all of them are incommunicable. But so are us.");
    next;
    mesn;
    mesq l("So, please, make the most of your stay and our leather works before this knowledge disappear from the world!");
    close;

OnInit:
    .distance = 4;
    .sex = G_MALE;
    end;
}

