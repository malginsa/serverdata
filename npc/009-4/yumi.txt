// TMW2 Script
// Author: Jesusalva
//  Description:
//    Joaquim's wife.
//    TODO sprite on the bed

009-4,36,33,0	script	Yumi	NPC_FEMALE,{
    .@q=getq(HalinarzoQuest_SickWife);
    if (.@q > 3) goto L_PostAid;
    if (.@q == 3) goto L_DoIt;
    if (BaseLevel > 36) goto L_PreAid;

    mesn;
    mesq l("Hello. Take care with the Mountain Snakes, they're highly poisonous!");
    close;

L_PreAid:
    mesn;
    mesq l("Please... Speak low... I am dying........");
    next;
    mesn;
    mesq l("The nurse... The nurse is... Doing all she can... To help me, though.");
    close;

L_PostAid:
    mesn;
    mesq l("Ah, I am lively again. I wish I could just raise from this bed and do some exercise, but the Nurse refuses to let me go.");
    // There could be another quest stage her
    close;

L_DoIt:
    if (countitem(ElixirOfLife) < 1)
            goto L_PreAid;

    inventoryplace ElixirOfLife, 1;
    delitem ElixirOfLife, 1;
    getexp 1000, 0;
    setq HalinarzoQuest_SickWife, 4;
    mesc l("*gulp* *gulp* *gulp*");
    next;
    mesn;
    mesq l("AAAAAAAHHHHH, Thanks, I am lively again!");
    next;
    mesn;
    mesq l("In fact, I am cured! Hooray!!");
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, NPCEyes);
    setunitdata(.@npcId, UDT_HEADMIDDLE, VneckJumper);
    setunitdata(.@npcId, UDT_HEADBOTTOM, RaidTrousers);
    setunitdata(.@npcId, UDT_WEAPON, CottonGloves); // Do not use boots when lying on the bed
    setunitdata(.@npcId, UDT_HAIRSTYLE, 10);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 15);

    .sex = G_FEMALE;
    .distance = 5;
    end;
}
