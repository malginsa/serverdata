// TMW2 Scripts.
// Author:
//  Jesusalva
// Description:
//  Monster King Throne ($@MK_CHALLENGE)

boss,45,45,0	script	#monsterthrone	NPC_HIDDEN,0,0,{
    end;

OnTouch:
    if (strcharinfo(2) == "Monster King") end;
    warp "boss", 45, 48;
    percentheal -15,0;
    dispbottom "The throne is cursed, only the Monster King may seat on it.";
    end;

// Controls the Event
OnBegin:
    .CYCLES=0;
    $@MK_CHALLENGE=true;
    .MK=monster("boss", 45, 45, "The Monster King", MonsterKing, 1, .name$+"::OnVictory");
    .@bhp=getunitdata(.MK, UDT_MAXHP);
    setunitdata(.MK, UDT_MAXHP,    .@bhp+2000*.FAILS);
    setunitdata(.MK, UDT_HP,       .@bhp+2000*.FAILS);

    // Spawn reinforcements
    .@mobId=MonsterLieutenant;
    .@ts$="Lieutenant";
    monster("boss", 40, 40, strmobinfo(1, .@mobId), .@mobId, 1);
    monster("boss", 50, 50, strmobinfo(1, .@mobId), .@mobId, 1);
    monster("boss", 40, 50, strmobinfo(1, .@mobId), .@mobId, 1);
    monster("boss", 50, 40, strmobinfo(1, .@mobId), .@mobId, 1);

    mapannounce("boss", "Begin!", bc_map|bc_npc);
    initnpctimer;
    end;

function CheckFinalAssault {
    if (!siege_calcdiff("boss", 5)) {
        kamibroadcast("You noobs, you all deserve to die!", "Monster King");
        stopnpctimer;
        $@MK_CHALLENGE=false;
        // Clean Up
        mapwarp("boss", "017-1", 120, 88);
        killmonsterall("boss");
        // Raise difficulty
        .FAILS+=1;
        // Halt execution
        end;
    }
    return;
}

OnTimer120000:
    .CYCLES+=1;
    //areamonster("boss", 20, 20, 70, 70, "Monster King Slave", );
    // Spawn several monsters on the Boss Room every 2 minutes
    siege_cast("boss", .name$, .FAILS, TP_TULIM|TP_HURNS|TP_NIVAL);
    // Spawn an extra mini-boss at minutes: 10 and 30
    if (.CYCLES == 5) {
        .@mobId=MonsterColonel;
        .@ts$="Colonel";
        monster("boss", 45, 44, strmobinfo(1, .@mobId), .@mobId, 1);
    }
    if (.CYCLES == 15) {
        .@mobId=MonsterGeneral;
        .@ts$="General";
        monster("boss", 45, 44, strmobinfo(1, .@mobId), .@mobId, 1);
    }
    initnpctimer;
OnTimer15000:
    // Each fail raise curse duration in 0.1s - chance of curse is 15% each 15s
    if (rand2(100) <= 15) {
        areasc2("boss", 45, 45, 25, 3000+(.FAILS*100), SC_CURSE, BL_PC | BL_HOM | BL_MER);
        //globalmes("MSG");
        unittalk(.MK, "Be cursed, you fools! I am the mighty Monster King!!");
    }
OnTimer5000:
OnTimer10000:
OnTimer20000:
OnTimer25000:
OnTimer30000:
OnTimer35000:
OnTimer40000:
OnTimer45000:
OnTimer50000:
OnTimer55000:
OnTimer60000:
OnTimer65000:
OnTimer70000:
OnTimer75000:
OnTimer80000:
OnTimer85000:
OnTimer90000:
OnTimer95000:
OnTimer100000:
OnTimer105000:
OnTimer110000:
OnTimer115000:
    CheckFinalAssault();
    end;

// Monster King was defeated - game won
OnVictory:
    // Not killed by a player? It doesn't counts, then
    if (!playerattached())
        end;
    $@MK_CHALLENGE=false;
    kamibroadcast("has just defeated the Monster King.", strcharinfo(0));
    stopnpctimer;
    mapwarp("boss", "017-1", 120, 88);
    $GAME_STORYLINE=5;
    // Without the Monster King to rule monsters... TODO Isbamuth
    setbattleflag("monster_ai", 0x209);
    setbattleflag("monster_active_enable", false);
    setbattleflag("mob_count_rate", 25);
    //charcommand("@reloadbattleconf"); // Careful!
    donpcevent("@exprate::OnReload");
    donpcevent("@droprate::OnReload");
    // Player Reward
    getitembound(AegisShield, 1, 1);
    dispbottom l("For defeating the Monster King, you've got the Legendary @@.", getitemlink(AegisShield));
    dispbottom l("This item cannot be traded normally and is a Legendary Item.");
    dispbottom l("You can transfer it with \"@grantpower\" command. Please contact a GM for more info.");
    dispbottom l("Protip: If you plan in selling it, it's adviseable to ask for GM mediation.");
    end;
}

// Room Traps, only against players
boss,0,0,0	script	#MKBossTrap01	NPC_TRAP,0,0,{
    end;

OnTouch:
    SteelTrap(rand2(10, 20));
    end;

OnTimer10000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay .name$, NPC_TRAP;
    //end;
    // Move the trap away after it disarms

OnMinute14:
OnMinute26:
OnMinute40:
OnMinute54:
OnInit:
    .@x=rand2(20,70);
    .@y=rand2(20,70);
    movenpc .name$, .@x, .@y;
    end;
}

// Create more traps. (They can be on walls)
boss,0,0,0	duplicate(#MKBossTrap01)	#MKBossTrap02	NPC_TRAP,0,0
boss,0,0,0	duplicate(#MKBossTrap01)	#MKBossTrap03	NPC_TRAP,0,0
boss,0,0,0	duplicate(#MKBossTrap01)	#MKBossTrap04	NPC_TRAP,0,0
boss,0,0,0	duplicate(#MKBossTrap01)	#MKBossTrap05	NPC_TRAP,0,0


