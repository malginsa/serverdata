// TMW-2 Script
// Original Creator:  Adson Renato <adson25@gmail.com>
// Date:              2010-11-19 2:31 AM BRT
// Review:
//                    * 2013-09-03: ernando2000 <ernando.quirino@hotmail.com>
//                    * 2013-09-04: Lunovox Heavenfinder <rui.gravata@gmail.com>
//                    * 2016-06-14: Jesusalva <tmwbr@jesussalva.com>
//                    * 2017-06-17: Jesusalva <cpntb1@ymail.com>
//                    * 2018-05-24: Jesusalva <admin@tmw2.org>
//                      * Adapted from TMW-BR to TMW2
// Minimum Req.:      LVL 30
// Consumables:       Bandits drops: CoinBag, Dagger, Leather Shield, Empty Bottle
// Prize:             1 BurglarMask
// Description:
//                    A contrabandist which feeds Halinarzo's black market.
//                    Will later teach the Steal skill. (maybe)
// Notes:
//                    Blame Saulc.




009-1,24,105,0	script	Charles, Trader King	NPC_ORC,{
    .@q=getq(HalinarzoQuest_TraderKing);
    if (BaseLevel < 30) goto L_Weak;
    if (.@q == 1) goto L_Return;
    if (.@q == 2) goto L_End;
    goto L_Start;

L_Weak:
    mesn;
    mesq lg("Get out of here, weakling. You'll be killed by bandits.");
    close;

L_Start:
    mesn;
    mes lg(":> \"Hello, youngling...\"");
    next;
    mesn strcharinfo(0);
    menu
        l("This character person is strange I better get away..."), L_Distrust,
        l("Hmm, who are you?") , L_WhoAmI;

L_WhoAmI:
    mes "";
    mesn;
    mesq l("I am the Trader King, because I supply a market!"); // the Black Market
    close;

L_Distrust:
    mes "";
    mesn strcharinfo(0);
    mes l("¬.¬ \"Hmm, to me, you look like a thief or bandit...\"");
    next;
    mesn;
    mes l(":D \"HAHAHAHAH! Me?! a thief? only during free time...\"");
    next;
    mesn strcharinfo(0);
    mes l("¬.¬ \"I knew it! I'll report you at once.\"");
    next;
    mesn;
    mes l("0.0 \"No no, please no! I can propose you a great deal for your silence!\"");
    next;
    menu
    l("Which deal?"), L_Deal,
    l("No, justice must be done.") , L_Die;

L_Deal:
    mes "";
    mesn;
    mesq l(":/ \"Hmm... let me see... Aha, I know! I can make you a great bandit with a simple burglar mask!\"");
    next;
    mesn strcharinfo(0);
    mes l(":o \"What? I'm not a bandit! But it looks SO COOL! Do you need anything while you're here?\"");
    next;
    mesn;
    mes l(":D \"Ah! Yes, that would help a lot! I don't need anything a bandit couldn't give you: 25 @@, 5 @@, 2 @@ and a @@. And 6000 GP.\"", getitemlink(CoinBag), getitemlink(EmptyBottle), getitemlink(Dagger), getitemlink(LeatherShield));
    mesq l("What do you say?");
    next;
    mesn strcharinfo(0);
    menu
    l("No way! That's absurd!"),       L_Close,
    l("That's a lot, but we have a deal!"), L_Accept;

L_Accept:
    mes "";
    mesn;
    mes l(">:| \"Go take the items and remember: Not even a single word about me!\"");
    next;
    mesn strcharinfo(0);
    mes l(";-) \"Don't worry! Leave to me!\"");
    next;
    mesn;
    mes l("Here's what I need:");
    mes l("@@/25 @@", countitem(CoinBag), getitemlink(CoinBag));
    mes l("@@/5 @@", countitem(EmptyBottle), getitemlink(EmptyBottle));
    mes l("@@/2 @@", countitem(Dagger), getitemlink(Dagger));
    mes l("@@/1 @@", countitem(LeatherShield), getitemlink(LeatherShield));
    mes l("@@/6000 GP", Zeny);

    setq HalinarzoQuest_TraderKing, 1;
    close;

L_Return:
    mesn;
    mes "\"Do you have what I asked for?\"";
    next;
    menu
    l("Yes, of course."),               L_Finish,
    l("I forgot what you need."),       L_Review,
    l("Not yet, I'll be right back."),  L_Wait;

L_Wait:
    mes "";
    mesn;
    mesq l("I'll be waiting for you! And trading some items of questionable origins...");
    close;

L_Finish:
    if (countitem(CoinBag) < 25 ||
        countitem(EmptyBottle) < 5 ||
        countitem(Dagger) < 2 ||
        countitem(LeatherShield) < 1)
            goto L_Missing;
    if (Zeny < 6000) goto L_Poor;

    inventoryplace BurglarMask, 1;
    delitem CoinBag, 25;
    delitem EmptyBottle, 5;
    delitem Dagger, 2;
    delitem LeatherShield, 1;
    Zeny = Zeny - 6000;
    getitem BurglarMask, 1; // Bragging Rights
    getexp 5840, 25;
    setq HalinarzoQuest_TraderKing, 2;
    mes "";
    mesn;
    mes l(":D \"Excellent! You've kept your end on the bargain!\"");
    next;
    mesn;
    mesq l("I always keep my end on the bargain, so here you go.");
    next;
    mesn;
    mes l(":) \"A pleasure to trade if you. Don't get caught if you're doing something wrong!\"");
    close;

L_Poor:
    mes "";
    mesn;
    mes l(":< \"Hey hey! Where's the money?\"");
    close;

L_Missing:
    percentheal -15, 0;
    mes "";
    mesn;
    mes l(":< \"Never lie to me. Keep your end on the bargain! Give me everything I asked for!\"");
    close;

L_End:
    mesn;
    mes l(":P \"I am busy, leave me alone.\"");
    close;

L_Review:
    mes "";
    mesn;
    mes l("Here's what I need:");
    mes l("@@/30 @@", countitem(CoinBag), getitemlink(CoinBag));
    mes l("@@/5 @@", countitem(EmptyBottle), getitemlink(EmptyBottle));
    mes l("@@/2 @@", countitem(Dagger), getitemlink(Dagger));
    mes l("@@/1 @@", countitem(LeatherShield), getitemlink(LeatherShield));
    mes l("@@/6000 GP", Zeny);
    close;

L_Die:
    mesn;
    mesq l("Then I'll be sure you can't talk again.");
    die();
    close;

L_Close:
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, BurglarMask);
    setunitdata(.@npcId, UDT_HEADMIDDLE, Chainmail);
    setunitdata(.@npcId, UDT_HEADBOTTOM, RaidTrousers);
    setunitdata(.@npcId, UDT_WEAPON, LeatherBoots); // Boots
    setunitdata(.@npcId, UDT_HAIRSTYLE, 7);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 1);

    .sex = G_MALE;
    .distance = 5;
    end;


}
