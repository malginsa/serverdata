// TMW2 Script
// Author:
//    Jesusalva

006-4-1,24,53,0	script	#jakWestWind	NPC_HIDDEN,0,0,{
    end;

OnTouch:
    dispbottom l("This passage is still sealed.");
    end;
}

006-4-1,69,158,0	script	Abandoned Diary#jak1	NPC_NO_SPRITE,{
    setpcblock(PCBLOCK_HARD, true);
    mesc l("STORY MODE ENABLED. Monsters won't attack you, so you can read without worries."), 1;
    next;
    clear;
    mesc l("This diary had several pages torn off, but the last page is clearly legible.");
    mes "";
    mesn;
    mes l("It has been a total disaster. They killed %s. And everyone on the village.", b(l("her")));
    next;
    mes l("Why did we had to decide to research slimes of all creatures?! But alas, now is too late. Not even a %s could restore her in this state...", getitemlink(ElixirOfLife));
    next;
    mes l("So I did what I could - What I always do. Turned my beloved sister in a slime. Turned her into what killed her.");
    next;
    mes l("There is someone who might know how to turn a slime back in a human. But alas, that old fart doesn't live here.");
    next;
    mes l("Therefore, I shall head to the %s, and look for %s. If anyone knows how to fix it, might as well be that rat-faced... being.", b(l("Mirror Lake")), b(l("Wushin")));
    next;
    mes l("If you're reading this - Feel free to use my laboratory in any way you deem fit. Or what's left of it, at least. Also, please kill any slimes you find. I shall be back in six leaps time, according to the Imperial Time, but if I'm not back by then, assume something went wrong and both wushin and me are off the table.");
    next;
    mes l("JAK1, THE SLIME RESEARCHER");
    mes l("Oranye Isles, 257 AT");

    next;
    setpcblock(PCBLOCK_HARD, false);
    if (getvaultid())
        ##02_MLQUEST = ##02_MLQUEST | MLP_ML_JAK1;
    compareandsetq CandorQuest_Jak1, 0, 1;
    closeclientdialog;
    close;

OnInit:
    .distance=2;
    end;
}


