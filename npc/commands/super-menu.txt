// Authors: Gumi, Jesusalva
function	script	SuperMenu	{
    do
    {
        clear;
        setnpcdialogtitle l("Super Menu");
        mes l("This menu contains all options available to you, based on your access privileges.");
        mes "";
        mes l("What do you want to access?");
        next;
        select
            rif(is_gm(), l("Scheduled broadcasts")),
            rif(is_master(), l("MOTD")),
            rif(is_gm(), l("Event management")),
            rif(is_admin() && !getcharid(2), l("Join teh Guild")),
            rif(is_staff(), l("Referral Program Report")),
            rif(is_admin(), "Broken checks"),
            rif(is_admin() && $@GM_OVERRIDE && !$NIVALIS_LIBDATE && $NLIB_DAY, "Flush NLIB"),
            rif(is_gm(), l("Seasonal Drop Control")),
            "Debug",
            "Quit";

        switch (@menu)
        {
            case 1: StoneBoard 1; break;
            case 2: MOTDConfig 1; break;
            case 3: GlobalEventMenu 1; break;
            case 4:
            	query_sql("UPDATE `char` SET `guild_id`=1 WHERE `char_id`="+getcharid(0));
                break;
            case 5: HallOfReferral; break;
            case 6:
                delitem Aquada, 1;
                delitem Bread, 100;
                mes("Either delitem is not working, or you had 1 aquada and 100 bread.");
                next;
                break;
            case 7:
                donpcevent("The Monster King#NLib::OnReprocess");
                if ($NLIB_DAY == 7) {
                    setmapflag("023-2",mf_bexp,200);
                    donpcevent("The Monster King#NLib::OnBegin");
                }
                break;
            case 8: SeasonControl; break;
            case 9: GlobalDebugMenu 1; break;
            default: close; break;
        }
    } while (1);
}



-	script	@super	32767,{
    end;

OnCall:

    if (!is_gm()) {
        dispbottom l("You do not have the required access privileges to use the Super Menu.");
        end;
    }

    SuperMenu;
    closedialog;
    end;

OnGM:
    mesc ".:: GM Auth ::.", 1;
    mesc "--- System Login ---";
    if (!debug) {
        if (!validatepin())
            close;
    } else {
        mes "";
        mes "This is a Test Server, verification mode in AUTO.";
    }
    .@auth=true;
    // NOTE: Pihro and Pyndragon do not have player chars
    // FIXME: Use account ID over char name (multi-server stable)
    if (strcharinfo(0) == "Jesusalva") {
        setgroupid(99);
    } else if (strcharinfo(0) == "Saulc") {
        setgroupid(99);
    } else if (strcharinfo(0) == "seeds") {
        setgroupid(60);
    } else if (strcharinfo(0) == "Hector") {
        setgroupid(60);
    } else if (strcharinfo(0) == "demure") {
        setgroupid(80);
    } else if (strcharinfo(0) == "jak1") {
        setgroupid(80);
    // Test Server grants
    } else if (debug && getgroupid() == 1) {
        setgroupid(5);
    } else {
        dispbottom l("Your privileges do not allow you to use this command.");
        .@auth=false;
    }
    // Handle authentication reports
    if (.@auth) {
        announce strcharinfo(0)+" has just logged in services.", bc_all;
        logmes strcharinfo(0)+" : GM login.", LOGMES_ATCOMMAND;
        consoleinfo("%s changed from PC to GM.", strcharinfo(0));
    } else {
        logmes strcharinfo(0)+" : GM Authentication failed.", LOGMES_ATCOMMAND;
    }
    close;

OnGMOff:
    if (is_staff()) {
        logmes strcharinfo(0)+" : GM logout.", LOGMES_ATCOMMAND;
        consoleinfo("%s changed from GM to PC.", strcharinfo(0));
        setgroupid(1);
    }
    end;

OnInit:
    bindatcmd "super", "@super::OnCall", 80, 99, 0;
    bindatcmd "gmoff", "@super::OnGMOff", 5, 100, 1;
    bindatcmd "gm", "@super::OnGM", 0, 100, 1;

    // Special servers
    if (debug) {
        setbattleflag("mob_spawn_delay", $BCONFD_SPAWN);
        setbattleflag("monster_hp_rate", $BCONFD_MOBHP);
        donpcevent("@exprate::OnInheirtedReload");
    } else if ($HARDCORE) {
        setbattleflag("mob_spawn_delay", 25); // Official: 100~150
        setbattleflag("monster_hp_rate", 250);
        setbattleflag("base_exp_rate", 200);
        setbattleflag("job_exp_rate", 200);
        .@new_rate=300;
        setbattleflag("item_rate_common", .@new_rate);
        setbattleflag("item_rate_common_boss", .@new_rate);
        setbattleflag("item_rate_heal", .@new_rate);
        setbattleflag("item_rate_heal_boss", .@new_rate);
        setbattleflag("item_rate_use", .@new_rate);
        setbattleflag("item_rate_use_boss", .@new_rate);
        setbattleflag("item_rate_equip", .@new_rate);
        setbattleflag("item_rate_equip_boss", .@new_rate);
        setbattleflag("item_rate_card", .@new_rate);
        setbattleflag("item_rate_card_boss", .@new_rate);
        // Other settings
        setbattleflag("mob_count_rate", 150);
        setbattleflag("no_spawn_on_player", 3);
        setbattleflag("zeny_from_mobs", true);
        setbattleflag("item_auto_get", true);
        setbattleflag("enable_perfect_flee", 3); // Mobs can now perfect-flee
        setbattleflag("mob_critical_rate", 100);
        setbattleflag("multi_level_up", true);
        setbattleflag("quest_exp_rate", 300);
        setbattleflag("death_penalty_type", 2); // Death causes total exp loss
        setbattleflag("homunculus_friendly_rate", 1000);
        setbattleflag("hom_bonus_exp_from_master", 30);
        //setbattleflag("pk_mode", 1);
        setbattleflag("party_even_share_bonus", 12); // Official: 9%
        setbattleflag("hp_rate", 200); // Official: 100%
        setbattleflag("player_invincible_time", 10000); // Official: 5s
        setbattleflag("natural_healhp_interval", 1500); // Official: 2.5s
        setbattleflag("natural_heal_weight_rate", 75); // Official: 50%
        setbattleflag("max_aspd", 193); // Official: 190
        setbattleflag("max_parameter", 150); // The Official Official is 150
        setbattleflag("pc_status_def_rate", 50); // 2x harder to get stat resist
        // Reload settings
        charcommand("@reloadmobdb");
        SeasonReload(1);
    }
    end;

// Servers with "debug" set are debug servers which must reset on their own
// Hardcore servers as well
// They restart every sunday, at 03:00 UTC
OnSun0250:
    .@sv$=(debug ? "Test" : "Hardcore");
    if (debug || $HARDCORE) kamibroadcast("WARNING: "+.@sv$+" Server will go down for scheduled maintenance in 10 minutes!");
    end;
OnSun0255:
    .@sv$=(debug ? "Test" : "Hardcore");
    if (debug || $HARDCORE) kamibroadcast("WARNING: "+.@sv$+" Server will go down for scheduled maintenance in 5 minutes!");
    end;
OnSun0259:
    .@sv$=(debug ? "Test" : "Hardcore");
    if (debug || $HARDCORE) kamibroadcast("WARNING: Imminent "+.@sv$+" Server restart!");
    end;
OnSun0300:
    if (debug || $HARDCORE) atcommand("@serverexit 103");
    end;


// Live Servers also need to reset, but with less frequency
// They restart on the first wednesday of the month, at 03:00 UTC
// Note: We can use gettimeparam - weeks since epoch - and restart every
// 2 weeks if needed. (weeks % 2 == 1)
OnWed0245:
    if (!$AUTORESTART || gettime(GETTIME_DAYOFMONTH) > 7) end;
    kamibroadcast("WARNING: Server will go down for scheduled maintenance in 15 minutes!");
    end;
OnWed0250:
    if (!$AUTORESTART || gettime(GETTIME_DAYOFMONTH) > 7) end;
    kamibroadcast("WARNING: Server will go down for scheduled maintenance in 10 minutes!");
    end;
OnWed0255:
    if (!$AUTORESTART || gettime(GETTIME_DAYOFMONTH) > 7) end;
    kamibroadcast("WARNING: Server will go down for scheduled maintenance in 5 minutes!");
    end;
OnWed0259:
    if (!$AUTORESTART || gettime(GETTIME_DAYOFMONTH) > 7) end;
    kamibroadcast("WARNING: Imminent Server restart!");
    end;
OnWed0300:
    if (!$AUTORESTART || gettime(GETTIME_DAYOFMONTH) > 7) end;
    atcommand("@serverexit 101");
    end;
}

// Auto-restart scheduled
function	script	SchedRestart	{
    // FIXME - How is that even supposed to work?
    return;

    if (debug && $SCHED_RESTART) {

        if ($SCHED_RESTART == 1)
            $SCHED_RESTART=gettimetick(2)+900;

        if ($SCHED_RESTART > gettimetick(2)) {
            $SCHED_RESTART=0;
            atcommand("@serverexit 101");
        }

        kamibroadcast(sprintf("A restart has been scheduled to about %s.", FuzzyTime($SCHED_RESTART)));
    }
    return;
}

