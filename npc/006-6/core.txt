// TMW2 Script
// Author:
//    Jesusalva

006-6,46,26,0	script	Magic Barrier#0066	NPC_HIDDEN,0,0,{
    end;

OnTouch:
    if (!(getq(General_EasterEggs) & EE_SNOWMAN)) goto L_Reject;
    if (SCANDORPTS < 300) goto L_Reject;
    if (CAPTURE_FLAG < 1) goto L_Reject;
    if (getq(General_Narrator) < 17) goto L_Reject; // Frostia Arc must be finished
    /* Some of previous sections come back to bite you, now */
    if (CRAZYPOINTS < 10) goto L_Reject;

    specialeffect(5000, SELF, getcharid(3));
    dispbottom l("The power which lies in Candor acknowledges your strength.");
    sleep2(2500);
    warp "006-10", 42, 48;
    sleep2(500);
    specialeffect(5001, SELF, getcharid(3));
    end;

L_Reject:
    specialeffect(5000, SELF, getcharid(3));
    dispbottom l("The power which lies in Candor rejects your strength.");
    sleep2(3000);
    specialeffect(5002, SELF, getcharid(3));
    end;
}
