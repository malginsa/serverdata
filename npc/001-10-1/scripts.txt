// TMW2 scripts.
// Authors:
//    Jesusalva
//    Dusty in a Bottle (aka. Dustynator, Dustman, mr. willbelz)
// Description:
//    Scripts for 001-10-1: Boss Fight!

001-10-1,0,0,0	script	#COD_BossManager	NPC_HIDDEN,{
    end;

// Spawn the Dust Boss
OnEventStart:
    areamonster("001-10-1", 34, 29, 59, 52, strmobinfo(1, DustBoss), DustBoss, 1, "#COD_BossManager::OnVictory");
    initnpctimer;
    end;

// Dust boss was killed, add timers, special reward for victor
OnVictory:
    if (getcharid(1) > 0) {
        .pwin=getcharid(1);
        getpartymember(getcharid(1));
	    .@count = $@partymembercount;
        .bonus=max(0, getmapusers("001-10-1")-.@count);
    } else {
        .win$=strcharinfo(0);
        .bonus=0;
    }

    getitem BottledDust, 1;

    maptimer("001-10-1", 100, "#COD_BossManager::OnReward1");
    maptimer("001-10", 100, "#COD_BossManager::OnReward2");
    donpcevent("Colonel DUSTMAN::OnCoDEnd");
    end;

// Handle rewards: 10 dust for winner party on boss room, 1 dust for everyone else
// on boss room, 1 dust for winner party outside boss room
OnReward1:
    if ((.pwin > 0 && getcharid(1) == .pwin) || strcharinfo(0) == .win$)
        getitem BottledDust, 9+.bonus;
    else
        getitem BottledDust, 1;
    // FALLTHROUGH
OnReward2:
    if (.pwin && getcharid(1) == .pwin) {
        setq2 LoFQuest_COD, getq2(LoFQuest_COD) + 1;
        getitem BottledDust, 1;
    }
    warp "018-2-1", 24, 29;
    end;

// You ran out of time
OnTimeDefeat:
    @COD_CHECKPOINT=0;
    if (getmap() ~= "001-10") {
        warp "018-2-1", 24, 29;
        dispbottom l("COD: Ran out of time!");
    }
    end;

// Must reset for next challenge
OnCleanUp:
    .pwin=0; // party winner
    .win$=""; // fallback, if char not in party
    killmonsterall("001-10-1");
    initnpctimer;
    stopnpctimer;
    end;

// Every 20 seconds mobs are created if needed
OnTimer20000:
    .@mi=mobcount("001-10-1", "all");
    .@pl=getmapusers("001-10-1");
    .@monsterId=any(DustRifle, DustGatling, DustRevolver);
    if (.@pl > (.@mi-2))
        areamonster("001-10-1", 34, 29, 59, 52, strmobinfo(1, .@monsterId), .@monsterId, (.@pl-.@mi+2));
    initnpctimer;
    end;

// Setup
OnInit:
    .pwin=0; // party winner
    .win$=""; // fallback, if char not in party
    end;
}


