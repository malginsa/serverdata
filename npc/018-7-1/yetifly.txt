// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//    Yetifly the Mighty

018-7-1,54,78,0	script	Yetifly	NPC_YETIFLY,{
    mesn;
    mesq l("Heh, congratulations making it this far. Once again, allow me to present myself:");
    next;
    mesn;
    mesq l("I am Yetifly the Mighty, guardian of the fae!");
    close;

OnInit:
    .distance = 4;
    .sex = G_MALE;
    end;
}

