// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 027-1: The Academy Island mobs
027-1,77,95,4,3	monster	Duck	1029,2,60000,30000
027-1,29,146,10,10	monster	Magic Ratto	1126,3,60000,30000
027-1,57,127,9,3	monster	Mana Piou	1155,3,60000,30000
027-1,114,116,30,25	monster	Red Butterfly	1025,7,60000,30000
027-1,128,25,12,6	monster	Duck	1029,2,30000,30000
027-1,150,48,10,30	monster	Croc	1006,6,30000,30000
027-1,59,116,24,25	monster	Mana Bug	1075,4,60000,30000
027-1,100,152,60,7	monster	Sea Slime	1093,12,5000,5000
027-1,149,117,11,27	monster	Piou	1002,4,60000,30000
027-1,89,65,26,25	monster	Cyan Butterfly	1172,6,60000,30000
