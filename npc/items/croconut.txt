// Evol scripts.
// Authors:
//    4144
//    Reid
//    Jesusalva
// Description:
//    Allows to break a Croconut into multiple parts.

-	script	Croconut	NPC_HIDDEN,{
    close;

OnUse:
    mesc l("Do you want to break open this %s?", getitemlink(Croconut));

    select
        l("Yes."),
        l("No.");
    mes "";
    closeclientdialog;
    switch (@menu) {
    case 1:
        goto L_Weapon;
    case 2:
        getitem Croconut, 1;
        close;
    }
    close;

L_Weapon:
    .@r=rand2(1,5);
    switch (.@r) {
        case 1:
        case 2:
        case 3:
            goto L_TooWeak; break;
        case 4:
            goto L_Weak; break;
        case 5:
            goto L_Good; break;
    }

L_TooWeak:
    // Croconuts do not heal much. So opening them without fail should be possible at relatively low strength levels.
    .@q = rand2(5);
    if (readparam2(bStr) > 10)
        .@q = .@q + 1;
    if (readparam2(bStr) > 25)
        .@q = .@q + 1;
    if (readparam2(bStr) > 35)
        .@q = .@q + 1;

    if (.@q == 0) goto L_TooWeakLost;
    if ( (.@q == 1) || (.@q == 2) ) goto L_TooWeakFail;
    if ( (.@q >= 3) && (.@q <= 6) ) goto L_Weak;
    if ( (.@q > 6) ) goto L_Good;

L_TooWeakLost:
    dispbottom l("Oops! You destroyed your %s.", getitemlink(Croconut));
    close;

L_TooWeakFail:
    dispbottom l("Well... you did not succeed in opening this %s.", getitemlink(Croconut));

    getitem Croconut, 1;
    close;

L_Weak:
    dispbottom l("You broke the %s into two parts, but you crushed one of them.", getitemlink(Croconut));

    getitem HalfCroconut, 1;
    close;

L_Good:
     dispbottom l("You perfectly cut your %s into two edible parts.", getitemlink(Croconut));

    getitem HalfCroconut, 2;
    close;
}
