// TMW2 Script.
// Author:
//    Jesusalva
// Description:
//    Important Sign

022-1,38,44,0	script	Sign#02213844	NPC_SWORDS_SIGN,{
    mesc b(l(".:: PVP King Imperial Arena ::."));
    mesc l("The Arena is currently closed for maintenance.");
    mesc l("It's advised to collect PVP equipment.");
    mesc l("If you think this should be a priority, please ask Jesusalva."), 1;
    tutmes l("If you kill an opponent stronger than you, you will gain honor points. But if the oponent is 15 levels weaker than you, it will be NEGATIVE!"), l("About Scoreboards and Honor Points");
    tutmes l("You will also LOSE honor if the opponent is below level 30. If you are a bandit (negative honor), all fights versus you will be honorable."), l("About Scoreboards and Honor Points");
    tutmes l("If you kill the same person within 30 minutes, honor will not fluctuate. The whole honor system is very experimental."), l("About Scoreboards and Honor Points");
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 3;
    end;
}
