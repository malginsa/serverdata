// TMW2/LoF scripts.
// Authors:
//    TMW BR Team
//    Jesusalva
// Description:
//    Exchanges Mountain Snake Plate (TBD) for a Nymph Necklace (TBD)
//    Grand Hunter Quest

018-5-2,33,36,0	script	Leona	NPC_FAIRY_B,{
    function leona_exchange;
    mesn;
    mesq l("Hello, @@!", get_race());
    next;
    mesn;
    mesq l("Do you have something to exchange with me? Or perhaps you want a Grand Hunter Quest?");
    next;
    select
        l("I've brought something to exchange."),
        l("I'm interested in Grand Hunter Quest."),
        l("Ops, sorry. I was going to the Soul Menhir and entered your house by accident.");
    mes "";
    switch (@menu) {
    case 3:
        mesn;
        mesq l("It happens.");
        close;
    case 2:
        GHQ_Assign(MountainSnake, "Lilit");
        close;
    case 1:
        mesn;
        mesq l("The most famous nymphs, are those who wear stuff made of Snake Skin.");
        next;
        mesn;
        mesq l("Perhaps you have something like that?");
        next;
        do
        {
            mesc l("What to exchange with Leona?");
            mes "##B" + l("Drag and drop an item from your inventory.") + "##b";

            .@id = requestitem();

            // If ID is invalid, there's not enough items or if it is bound
            if (.@id < 1) close;
            if (countitem(.@id) < 1) close;
            if (checkbound(.@id))
            {
                mesc l("You cannot part with this item!");
                continue;
            }

            // TODO: Check if item is OK
            switch (.@id) {
            // Specific Exchange
            /*
            case Backsword:
                leona_exchange(.@id, ShortSword);
                break;
            */

            // Generic Exchange
            case LeatherShirt:
            case LeatherBoots:
            case LeatherGloves:
            case JeansChaps:
            case LeatherTrousers:
            case SnakeSkin:
            case MountainSnakeSkin:
            case CaveSnakeSkin:
                leona_exchange(.@id, 0);
                break;
            case BlackMambaSkin:
                leona_exchange(.@id, FluoPowder);
                break;
            default:
                mesn;
                mesq l("I have no interest on this item.");
                next;
                break;
            }

        } while (true);
    }
    close;

    // leona_exchange ( give, receive )
    // Receive should be item ID. If it is 0, you will get 1.4× the sell price
    function leona_exchange {
        .@what=getarg(0);
        .@reward=getarg(1);
        if (!.@reward)
            .@gp=getiteminfo(.@what, ITEMINFO_SELLPRICE)*14/10;
        mesn;
        if (.@reward)
            mesq l("For this @@, I offer you a(n) @@.", getitemlink(.@what), getitemlink(.@reward));
        else
            mesq l("For this @@, I offer you @@ GP.", getitemlink(.@what), format_number(.@gp));
        next;
        mesc l("Exchange the item with Leona?");
        if (askyesno() == ASK_YES)
        {
            delitem .@what, 1;
            if (.@reward)
                getitem .@reward, 1;
            else
                Zeny+=.@gp;
            mesn;
            mesq l("Many thanks! I'll be sooooo fashionable now!");
            next;
        }
        return;
    }
OnInit:
    .distance=5;
    end;
}
