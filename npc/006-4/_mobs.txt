// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 006-4: Abandoned Island mobs
006-4,57,82,10,24	monster	Mana Ghost	1068,5,75000,25000
006-4,59,192,16,19	monster	Mana Ghost	1068,9,75000,250000
006-4,94,179,17,15	monster	Mana Ghost	1068,9,75000,250000
006-4,128,183,14,19	monster	Mana Ghost	1068,9,75000,250000
006-4,102,202,20,8	monster	Mana Ghost	1068,9,75000,250000
006-4,101,153,21,10	monster	Mana Ghost	1068,9,75000,250000
006-4,95,66,39,11	monster	Green Dragon	1195,6,75000,105000
006-4,31,251,17,12	monster	Green Slime Mother	1236,1,75000,25000
006-4,66,203,15,11	monster	Green Slime Mother	1236,3,75000,25000
006-4,139,154,14,9	monster	Green Slime Mother	1236,1,75000,25000
006-4,149,181,7,19	monster	Green Slime Mother	1236,1,75000,25000
006-4,60,149,11,12	monster	Green Slime Mother	1236,1,75000,25000
006-4,142,77,10,22	monster	Blue Slime Mother	1237,1,75000,25000
006-4,94,91,34,11	monster	Copper Slime Mother	1238,1,75000,25000
006-4,94,85,35,6	monster	Green Dragon	1195,6,75000,105000
006-4,55,97,12,9	monster	White Slime Mother	1242,1,120000,25000
