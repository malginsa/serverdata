// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 004-2-1: Canyon Cave mobs
004-2-1,0,0,0,0	monster	Cave Maggot	1027,25,40000,200000
004-2-1,67,48,29,23	monster	Cave Snake	1035,12,35000,150000
004-2-1,80,36,11,4	monster	Snake	1122,1,35000,150000
004-2-1,46,70,14,13	monster	Snake	1122,3,35000,150000
004-2-1,90,55,9,9	monster	Snake	1122,3,35000,150000
004-2-1,38,40,4,2	monster	Maggot	1030,2,35000,150000
004-2-1,57,24,4,2	monster	Maggot	1030,2,35000,150000
004-2-1,34,62,4,2	monster	Maggot	1030,2,35000,150000
004-2-1,88,68,4,2	monster	Maggot	1030,2,35000,150000
004-2-1,48,31,17,15	monster	Scorpion	1071,5,35000,150000
004-2-1,0,0,0,0	monster	Small Topaz Bif	1101,2,35000,150000
