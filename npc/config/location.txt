// TMW2 Script
// Author: Jesusalva
// Location Config

-	script	loc_config	32767,{
    end;

OnInit:
    // TP_FORT TP_BOSSR
    setarray $@LOCMASTER_TP,  TP_CANDOR,TP_TULIM,TP_HALIN,TP_HURNS,  TP_LOF,TP_LILIT,TP_NIVAL,  TP_FROST, TP_FORT,TP_ARTIS;
    setarray $@LOCMASTER_LOC$, "Candor", "Tulim", "Halin", "Hurns",   "LoF", "Lilit", "Nival", "Frostia",  "Fort", "Artis";
    setarray $@LOCMASTER_MAP$,  "005-1", "003-1", "009-1", "012-1", "017-1", "018-5", "020-1",   "024-1", "025-2", "029-0";
    setarray $@LOCMASTER_X,          43,      40,      26,      86,     119,     111,      56,        94,     108,     203;
    setarray $@LOCMASTER_Y,          99,      49,      30,      69,      87,      53,      62,        42,      32,      85;

    //debugmes "Locmaster: Index 0: %s [%s.gat (%d, %d)]", $@LOCMASTER_LOC$[0], $@LOCMASTER_MAP$[0], $@LOCMASTER_X[0], $@LOCMASTER_Y[0];
    //debugmes "Locmaster: Index 2: %s [%s.gat (%d, %d)]", $@LOCMASTER_LOC$[2], $@LOCMASTER_MAP$[2], $@LOCMASTER_X[2], $@LOCMASTER_Y[2];
    //debugmes "Locmaster: Index 5: %s [%s.gat (%d, %d)]", $@LOCMASTER_LOC$[5], $@LOCMASTER_MAP$[5], $@LOCMASTER_X[5], $@LOCMASTER_Y[5];
    end;
}

// Resaves your respawn point
function	script	ResaveRespawn	{
    .@i=array_find($@LOCMASTER_LOC$, LOCATION$);
    savepoint $@LOCMASTER_MAP$[.@i], $@LOCMASTER_X[.@i], $@LOCMASTER_Y[.@i];
    return;
}

// Warps you to last visited town
function	script	ReturnTown	{
    .@i=array_find($@LOCMASTER_LOC$, LOCATION$);
    warp $@LOCMASTER_MAP$[.@i], $@LOCMASTER_X[.@i], $@LOCMASTER_Y[.@i];
    return;
}

