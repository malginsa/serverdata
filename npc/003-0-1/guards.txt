// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Magic Council Guard

003-0-1,50,24,0	script	Guard#00301A	NPC_BRGUARD_SPEAR,{
    if (getgmlevel())
        goto L_Sponsor;
    if ($FIRESOFSTEAM)
        goto L_Steam;

    mesn;
    mesq l("Past this grates, is the Magic Council Room.");
    next;
    mesn;
    mesq l("Unless you're an Alliance member or have authorization, you cannot go in.");
    if (getvaultid())
        mesc l("Alliance members are those who [@@https://tmw2.org/contact|sponsor us@@].");
    else
        mesc l("Alliance members are those who [@@https://patreon.com/TMW2|sponsor us@@].");
    mesc l("There's nothing in the Magic Council Room, though. It's just a perk.");
    close;

L_Sponsor:
    mesn;
    mesq l("Alliance members (sponsors) are allowed inside the Council Room.");
    mesc l("Warp to Council room?");
    if (askyesno() == ASK_YES) {
        warp "003-0-2", 34, 42;
    }
    closeclientdialog;
    close;

L_Steam:
    mesn;
    mesq l("The Council is not in session, it has dispersed since Andrei Sakar went to an expedition to Artis. However, it was decided to allow others inside.");
    mesc l("Warp to Council room?");
    if (askyesno() == ASK_YES) {
        warp "003-0-2", 34, 42;
    }
    closeclientdialog;
    close;

OnInit:
    .distance=4;
    end;
}

