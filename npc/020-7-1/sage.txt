// TMW2 scripts.
// Authors:
//    Jesusalva
//    TMW Org.
// Description:
//    Blue Sage
//
//    Minimum level: 36 (implicit) -> 40 (presumed at quest end)
//    Minimum jblvl: 16 (implicit) -> 20 (presumed at quest end)
//
// NivalisQuest_BlueSage STRUCTURE
//  FIELD 1:
//      INVESTIGATION
//      1 - STBY OUTSIDE
//      2 - ACCESS GRANTED
//      3 - QUEST ASSIGNED BY PEETU - talk to Oskari (and others)
//      4 - Oskari is OK with peetu, but wanna hear from others. He also sends you
//          to ask what Peetu happened
//      5 - Adultered ingredients seems the cause, report to Elias
//      6 - Elias is now worried about a visitor. Ask people about and report.
//      7 - If everyone found the visitor, confirm Elias the worries
//      8 - Elias sent you to Oskari to inform the issue. Blue Sage probably knew all along.
//      8/9 WARNING: will not advance unless everyone thinks Peetu is good.
//      9 - Oskari accepts the cause. Tells to report Peetu that it probably was
//          a saboutage, to check if the Silk Cocoon really was there.
//      10 - Peetu confirmed the saboutage. Report to Blue Sage.
//      11 - Blue Sage accepted the evidence, and explains about other sages issues.
//          It's not known who or what is behind this. He excuses for making you waste
//          your time. He asks you to return to him later, as he needs to write letters.
//      12 - QUEST COMPLETE - You collected your reward
//          Also picked up a letter for Frostia Mayor, about the incident (Main Story).
//  FIELD 2:
//      Bitwise (BS_QVISITOR)
//  FIELD 3:
//      Bitwise (BS_QHELPER)

020-7-1,40,24,0	script	#BlueSageCaves	NPC_HIDDEN,1,0,{
OnTouch:
    .@q=getq(NivalisQuest_BlueSage);
    if (.@q < 12) {
        dispbottom l("The door is locked.");
    } else {
        warp "020-7-2", 71, 53;
    }
    end;
}

020-7-1,35,59,0	script	The Blue Sage	NPC_BLUESAGE,{
    function nStart;
    function nReport;
    function nLetters;
    function nCindy;

    .@cindy=($@CINDY_STATE < gettimetick(2));
    .@q=getq(NivalisQuest_BlueSage);

    // If you helped the four page makers, you receive a reward
    // I know a Titanium Ingot is lame, but in TMW Org. there was no reward *at all*
    .@qt=getq(NivalisQuest_BlueSagePagemaker);
    if (.@qt == 0) {
        .@qt2=getq2(NivalisQuest_BlueSagePagemaker);
        if (.@qt2 == BS_PMALL) {
            inventoryplace TitaniumIngot, 1;
            mesn l("Nikolai, the Blue Sage");
            mesq l("I've heard you helped my staff to recreate some books. For that, I am grateful.");
            next;
            mesn l("Nikolai, the Blue Sage");
            mesq l("Please accept this @@ as a gratitude for your time.", getitemlink(TitaniumIngot));
            getitem TitaniumIngot, 1;
            getexp 3535, 215; // The remaining 20% EXP to reach 100% =D
            setq1 NivalisQuest_BlueSagePagemaker, 1;
            next;
        }
    }

    mesn l("Nikolai, the Blue Sage");
    if (is_night())
        .@t$=l("Good evening");
    else
        .@t$=l("Good morning");
    mesq .@t$ + l(", my name is Nikolai. I am a sage, and the owner of this place.");
    next;
    mesn l("Nikolai, the Blue Sage");
    mesq l("Not only that, but I am also Angela's husband and Nivalis Mayor. If you have any issues, you can go straight to me.");
    if (.@cindy) {
        mesc l("Nikolai takes a sweat of his head. He seems worried with Cindy.");
    }
    next;
    select
        rif(.@q < 10, l("I came here to talk about the World's Edge.")),
        rif(.@q == 10, l("I came here to report... A sabotage.")),
        rif(.@q == 11, l("I'm back.")),
        rif(.@q >= 12, l("Can you repeat what you said before?")),
        rif(.@cindy, l("Aren't you Cindy's father? Why don't you go to her rescue?")),
        l("Please excuse me, Blue Sage Nikolai.");
    mes "";
    switch (@menu) {
        case 1:
            // You must solve the issue here, first
            nStart();
            break;
        // Blue Sage Investigation: Report about saboutage
        case 2:
            nReport();
            break;
        // Blue Sage Investigation: Post-Report
        case 3:
            if (@timed < gettimetick(2))
                nLetters();
            else
                mesq l("I'm not done yet, please hold tight. It won't take long.");
            break;
        // Repeat about WE and AFSM
        case 4:
            nLetters();
            break;
        // Question about Cindy
        case 5:
            // Quest ends at stage 12
            if (.@q != 12) {
                mesn l("Nikolai, the Blue Sage");
                mesq l("The house is a mess. They need me here. Also.");
                next;
            }
            nCindy();
            break;
    }
    close;

// Report about the sabotage
function nReport {
    mesc l("You explain the Blue Sage about the sabotage incident details, from the Silk Cocoon to the masked visitor.");
    next;
    mesn l("Nikolai, the Blue Sage");
    mesq l("Hm hm. Thanks for the report. Well, as you could have suspected... I knew that all along.");
    next;
    mesn l("Nikolai, the Blue Sage");
    mesq l("Listen, it was not only me who had issues with this... Masked Man. Auldsbel also had a similar issues.");
    next;
    mesn l("Nikolai, the Blue Sage");
    mesq l("In other words, whoever they are, they're after the sages. They are not targeting just simple magical users. This might be a problem.");
    next;
    mesn l("Nikolai, the Blue Sage");
    mesq l("I must, however, thank you. I could not just come straight up and tell that to everyone, nor simply lock the house without no reason. I'm always fair.");
    next;
    mesn l("Nikolai, the Blue Sage");
    mesq l("Your help was invaluable, my staff believed in the unbiased view of the Hurnscald Household. Alas, now Peetu is capable to work again.");
    next;
    mesn l("Nikolai, the Blue Sage");
    mesq l("You wanted to know about the World Edge, right? The Ancient Families of the Soul Menhir, and if you're part of them...");
    next;
    mesn l("Nikolai, the Blue Sage");
    mesq l("I cannot answer you about yourself, but I can tell you about the Ancient Families and the World Edge. Which is classified information, by the way.");
    next;
    mesn l("Nikolai, the Blue Sage");
    mesq l("However, you'll need to keep cooperating with me. Trust me, this incident and what you look for it is closely related.");
    next;
    mesn l("Nikolai, the Blue Sage");
    mesq l("I'll be writing a letter to the next town you're going to visit, and also assign work to my household. You should come back later.");

    getexp 17500, 1000;
    setq1 NivalisQuest_BlueSage, 11;
    @timed=gettimetick(2)+30;
    return;
}

// Receive the letters and the next task along a text wall about the AFSM and WE
function nLetters {
    .@q=getq(NivalisQuest_BlueSage);
    inventoryplace Coal, 20;
    mesn l("Nikolai, the Blue Sage");
    mesq l("So. For the info dump or text wall... I'll let you choose what you want to know, or to skip it entirely.");

    do {
        next;
        mesc l("What do you want to know?");
        mes "";
        select
            l("What's the World Edge?"),
            l("Where is the World's Edge?"),
            l("What are the Ancient Families of the Soul Menhir?"),
            l("About the Prophecy... What about Elves? Orcs? Redys? Etc.?"),
            l("That's everything I wanted to know.");
        mes "";
        switch (@menu) {
        case 1:
        mesn l("Nikolai, the Blue Sage");
        mesq l("World's Edge is the place where the Monster King Fortress is.");
        next;
        mesn l("Nikolai, the Blue Sage");
        mesq l("It is the place where it all began... And I'm not talking about the Mana War.");
        next;
        mesn l("Nikolai, the Blue Sage");
        mesq l("It is the birthplace of humans, the first place to come to existence... The World Edge. The place where humanity began, and according to the legend... The place where it shall perish.");
        next;
        mesn l("Nikolai, the Blue Sage");
        mesq l("Humans never built a settlement there. But the prophecy is there.");
        break;
        case 2:
        mesn l("Nikolai, the Blue Sage");
        mesq l("World's Edge is an island situated northwest of here.");
        break;
        case 3:
        mesn l("Nikolai, the Blue Sage");
        mesq l("Do you know what a Soul Menhir is?");
        next;
        mesn l("Nikolai, the Blue Sage");
        mesq l("It more than just a piece of Zealite Ore... It is a part of the world's heart.");
        next;
        mesn l("Nikolai, the Blue Sage");
        mesq l("The World's Heart is at World's Edge. The *real* Ancient Families are the ones who broke it in parts and brought it to each town.");
        next;
        mesn l("Nikolai, the Blue Sage");
        mesq l("Thanks to this, we can now respawn after death. But that's when the prophecy was told. The prophecy... Of the death of all humans. It gives me shivers.");
        next;
        mesn l("Nikolai, the Blue Sage");
        mesq l("What you refer as Ancient Families, are probably their descendants. There was nothing special about them, other than they promised to defend mankind of their own actions.");
        next;
        mesn l("Nikolai, the Blue Sage");
        mesq l("When they left to World Edge, they probably were looking for Mana Fragments. In other words... Soul Menhirs. Parts of the World Heart.");
        next;
        mesn l("Nikolai, the Blue Sage");
        mesq l("I don't know why they would repeat a past mistake. The Monster King is not human anymore. I also do not know what artifact they lost. We have more questions than certainty about this.");
        break;
        case 4:
        mesn l("Nikolai, the Blue Sage");
        mesq l("They will all perish, along most of wildlife, according to the legend passed down.");
        next;
        mesn l("Nikolai, the Blue Sage");
        mesq l("However, the legend said there was a way to prevent this disaster. A single way... Which the Ancient Families of Soul Menhir kept a secret passed down between generations.");
        next;
        mesn l("Nikolai, the Blue Sage");
        mesq l("Knowing that would make you a descendant of them. But eh, that's asking too much. You had amnesia, right? That was on the letter.");
        break;
        }
    } while (@menu != 5);

    // Quest complete? Don't continue. If needed, give a hint to players.
    if (.@q != 11) {
        mesn l("Nikolai, the Blue Sage");
        if (getq(HurnscaldQuest_Sagratha) == 1)
            mesq l("I'm worried with which Sage the masked man will aim next...");
        else
            mesq l("Always a pleasure to help.");
        close;
    }

    mesn l("Nikolai, the Blue Sage");
    mesq l("So, back to action! Finally. Your next destination is going to be Frostia Town.");
    next;
    mesn l("Nikolai, the Blue Sage");
    mesq l("Remember the masked man said he was from Frostia? Or masked woman, we don't know. You should inform their Mayor at once.");
    next;
    mesn l("Nikolai, the Blue Sage");
    mesq l("Also, I don't know what Sage will be their next target. I'm... A bit concerned with Sagratha. As she choose to live with wildlife and all, we sorta don't know a lot of what happens with her.");
    next;
    mesn l("Nikolai, the Blue Sage");
    mesq l("Sorry, I'm babbling. Deliver this letter to Frostia's King. He will know what to do and what you should do.");
    next;

    getitem Coal, 20;
    setq1 NivalisQuest_BlueSage, 12;
    setq General_Narrator, 11;
    mesn l("Nikolai, the Blue Sage");
    mesq l("Also, take this. It's time to you learn to craft your own weapon. Talk to Nicholas in Hurnscald forge to make an awesome @@. Or use it on some other craft, it is your choice.", getitemlink(Backsword));
    mesc l("Received @@ @@!", 20, getitemlink(Coal));
    mesc l("Access to basement was granted!");
    getvaultexp(10);
    return;
}

// You must solve any issue inside the household before advancing main story
function nStart {
    mesn l("Nikolai, the Blue Sage");
    mesq l("Well, I would love to, but the house is a mess.");
    next;
    mesn l("Nikolai, the Blue Sage");
    mesq l("All Peetu have been doing the past hours was crying, and crying, and crying some more. I can't barely sleep hearing his cries from my room.");
    next;
    mesn l("Nikolai, the Blue Sage");
    mesq l("Not only that, but he is the only one here besides me who can do magic. Without him, I have to work doubled.");
    next;
    mesn l("Nikolai, the Blue Sage");
    mesq l("He should be crying in the far northeast corner of this library. Can you go there to see him?");
    next;
    mesn l("Nikolai, the Blue Sage");
    mesq l("Once he resumes working confidently, we can go over this important subject.");
    next;
    mesc b(l(".:: Main Quest 5-1 ::.")), 3;
    mesc l("* Meet the Blue Sage"), 2;
    mesc l("* Aid the Blue Sage in getting Peetu back to action"), 9;
    return;
}

// Question about Cindy
function nCindy {
    mesn l("Nikolai, the Blue Sage");
    mesq l("Just like every cave below the woodlands are under the Terranite King domains, every land covered in snow is under the Yeti King's domains.");
    next;
    mesn l("Nikolai, the Blue Sage");
    mesq l("It would be unwise to pick a fight with the Yeti King himself. The whole town would suffer.");
    next;
    mesn l("Nikolai, the Blue Sage");
    mesq l("Not only that, but the Yeti King and me had an... incident, in times best forgotten. Let's not talk about it.");
    next;
    mesn l("Nikolai, the Blue Sage");
    mesq l("The situation is different if someone from Hurnscald went to her rescue. That's why I ask you to help my Cindy.");
    next;
    mesn l("Nikolai, the Blue Sage");
    mesq l("Please talk to my wife first, she knows more about the situation than I do.");
    next;
    mesq l("Also, Yetis can be crafty at times. I think someone on Hurnscald Household knew a lot about them, you might want to ask them if you ever feel struck.");
    return;
}

OnInit:
    .sex=G_MALE;
    .distance=5;
    npcsit;
    end;
}

