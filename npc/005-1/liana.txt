// TMW2 Script
// Author:
//  Jesusalva
//  Saulc
// Description:
//      Candor girl ask for maggot sliem every 6 hours
// Variable:
//      CandorQuest_Liana
// PS. Liana could (should) explain too the small cave north of there. It can be
// a place to run, or maybe monsters there are natural and protect them from the
// Mana Monsters, etc.

005-1,54,96,0	script	Liana	NPC_ELVEN_FEMALE,{

    mesn;
    //mesq l("Hurnscald is a large city. I'm sure glad I live in Candor because I know where everything's at.");
    mesq l("Monsters do not aim small towns like Candor. This city also comes with the plus that I know where everything's at.");
    if (BaseLevel >= 5) goto L_Menu;
    tutmes l("This quest will unlock at level 5."), l("Protip"), false;
    close;

L_Menu:
    mesn;
    mesq l("Are you enjoying yourself in Candor? Do you have any questions?");
    mes "";
    menu
        l("What can I do with Maggot Slime?"),L_Slime,
        l("What can I do with Bug Leg?"),L_Bug,
        l("No, thanks."),L_Close;

L_Slime:
    mes "";
    .@q=getq(CandorQuest_Liana);
    mesq l("I collect them.");
    tutmes l("Liana, like other NPCs, provide a repeatable quest. Once you finish it, you will be able to do it again, after waiting for a few hours.");
    next;
    if (.@q == 0) goto L_Quest;
    if (gettimetick(2) >= LIANA_TIMER + 60 * 60 * 6) goto L_Repeat;
    close;

L_Quest:
    mesq l("With this I make balls of slime for Candor's childs, they really like to play with them.");
    next;
    mesq l("Maybe you could bring me 5 @@? I will reward you for your effort.", getitemlink(MaggotSlime));
    mes "";
    menu
        rif(countitem(MaggotSlime) >= 5, l("Here they are!")), L_Finish,
        l("I'll get to it."), L_Close;
    close; // double sure

L_Repeat:
    mesq l("I am searching again maggot slime to craft more balls.");
    next;
    mesq l("Maybe you could bring me 10 sticky @@?", getitemlink(MaggotSlime));
    mes "";
    menu
        rif(countitem(MaggotSlime) >= 10, l("Here they are!")), L_Finish2,
        l("I'll get to it."), L_Close;
    close;

L_Finish2:
    delitem MaggotSlime, 10;
    getexp 21, 0;
    Zeny = (Zeny + 80); // 10*4 = 40 base
    setq CandorQuest_Liana, 1;
    set LIANA_TIMER, gettimetick(2);
    mes "";
    mesn;
    mesq l("Many, many thanks! I'm sure the children will love it!");
    close;

L_Finish:
    delitem MaggotSlime, 5;
    getexp 58, 7;
    Zeny = (Zeny + 30); // 5*4 = 20 base
    setq CandorQuest_Liana, 1;
    set LIANA_TIMER, gettimetick(2);
    mes "";
    mesn;
    mesq l("Many, many thanks! I'm sure the children will love it!");
    close;

L_Bug:
    mes "";
    mesq l("Ah, personally I don't use it?");
    next;
    mes l("She shakes her head.");
    next;
    if (getq(CandorQuest_Vincent) < 2) {
        mesq l("You should ask this question at Vincent.");
        next;
        mesq l("He is in the process of making a figurine made of bug leg.");
        next;
    } else {
        mesq l("If you have a @@, perhaps you could use it as a bait.", getitemlink(FishingRod));
        next;
        mesq l("I guess it can also be used at quests... Or sell to Juliet, on Nard's ship.");
        next;
    }
    mesq l("I hope my answer help you in your adventure!");
    next;
    mes l("she's smiling at you.");
    goto L_Close;

L_Close:
    closedialog;
    goodbye;
    close;
}

