// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//    Soul Menhir

005-1,43,97,0	script	Soul Menhir#candor	NPC_SOUL_MOSS,{
    @map$ = "005-1";
    setarray @Xs, 42, 43, 44, 42, 44, 42, 43, 44;
    setarray @Ys, 96, 96, 96, 97, 97, 98, 98, 98;
    @x = 0;
    @y = 0;
    callfunc "SoulMenhir";
    @map$ = "";
    cleararray @Xs[0], 0, getarraysize(@Xs);
    cleararray @Ys[0], 0, getarraysize(@Ys);
    @x = 0;
    @y = 0;
    close;
}
