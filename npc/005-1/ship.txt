// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    This script controls access to Nard's Ship, fixing variables.

005-1,50,117,0	script	CandorShip	NPC_HIDDEN,0,0,{

OnTouch:
    LOCATION$="Candor";
    goto L_Warp;

L_Warp:
    warp "002-3@"+LOCATION$, 31, 28;
    closedialog;
    close;
}
