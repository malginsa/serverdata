// TMW2 Script
// Author:
//   Unknown
// Description:
//   Saxso's widow. Asks new players to trigger a bug bomb in the storehouse.

005-1,43,85,0	script	Zegas	NPC_MONA,{
    function lagTutorial;
    .@q = getq(CandorQuest_Barrel);
    if (BaseLevel < 5)
        goto L_Weak;
    if (.@q == 1)
        goto L_Find;
    if (.@q == 2)
        goto L_Looking;
    if (.@q == 3)
        goto L_QuestEnd;
    if (.@q == 4)
        goto L_Thanks;
    goto L_Meet;

L_Weak:
    mesn;
    mesq l("Do not enter in this storehouse, the maggots there will kill you.");
    close;

L_Meet:
    mesn;
    mesq l("Hey do you have a second?");
    next;
    mesq l("The storehouse here is overrun with house maggots.");
    next;
    mesq l("Wouldn't you know it, the bug bomb Eomie gave us is in one of the store room barrels.");
    next;
    mesq l("Can you search ##Bthe barrels##b for the bug bomb and set it off when you find it?");
    setq CandorQuest_Barrel, 1;
    menu
        l("Sure."),L_Start,
        l("Maybe some other time."),L_Close;

L_Find:
    mesn;
    mesq l("The storehouse is still over run with house maggots.");
    next;
    mesq l("Please help me find the bug bomb Eomie gave us is in one of the store room barrels?");
    menu
        l("Sure."),L_Start,
        l("Maybe some other time."),L_Close;

L_Start:
    mesn;
    mesq l("Thanks, come back and see me once you found the bug bomb and set it off.");
    setq CandorQuest_Barrel, 2;
    lagTutorial();
    close;

L_Looking:
    mesn;
    mesq l("Still haven't found it? Well, keep looking the barrels at this storehouse. I know it's in there, somewhere.");
    if (TUTORIAL) {
        mesc l("Read the tutorial again?");
        if (askyesno() == ASK_YES)
            lagTutorial();
    }
    close;

L_QuestEnd:
    mesn;
    mesq l("From the smell I can see you found the bug bomb!");
    mesq l("Thanks once again, I know it's not much but here is @@ GP for your troubles.", 350);
    getexp 63, 0;
    getitem CandorShorts, 1;
    Zeny = (Zeny + 350);
    setq CandorQuest_Barrel, 4;
    close;

L_Thanks:
    mesn;
    mesq l("Thanks for Helping with clear out the store room!");
    if (!getq(CandorQuest_Chest)) {
        next;
        mesn;
        mesq l("Do you see that fancy house over there? My husband, @@, used to live there.", b(l("Saxso")));
        next;
        mesn;
        mesq l("Strangely enough, when he died, he became a ghost. This shouldn't happen, but yet...");
        next;
        mesn;
        mesq l("Anyway, conspiratory theories aside, his ghost is a powerful boss. I would not attack him if I were you!");
    }
    close;

L_Close:
    closedialog;
    goodbye;
    close;

function lagTutorial {
    // TUTORIAL block
    tutmes l("The bugs there can be pretty vicious. Do you know what's worse though? Lag.");
    tutmes l("By pressing @@, you can open the Shortcut menu. And by pressing @@, you open the skill menu!", b("F8"), b("F5"));
    tutmes l("If you drag the \"Resync\" skill from the skill list to the shortcut list, you'll be able to rapidly solve some lag issues!");
    tutmes l("Of course, lag might always be a problem. Remember to drag healing items to the shortcut list too. %%2");
    return;
}

OnInit:
    .sex = G_FEMALE;
    .distance = 4;
    end;
}

005-1,32,73,0	script	ZegasDebug	NPC_MONA,{
    showavatar NPC_MONA;  // this is handled by avatars.xml
    mesn;
    mesq l("Reset?");
    next;
    menu
        l("Yes."),  L_Reset,
        l("No."),   L_Close;

L_Reset:
    setq CandorQuest_Barrel, 0;
    STARTAREA = STARTAREA &~ (1 << 2);
    STARTAREA = STARTAREA &~ (1 << 3);
    STARTAREA = STARTAREA &~ (1 << 4);
    STARTAREA = STARTAREA &~ (1 << 5);
    STARTAREA = STARTAREA &~ (1 << 6);
    STARTAREA = STARTAREA &~ (1 << 7);
    STARTAREA = STARTAREA &~ (1 << 8);
    STARTAREA = STARTAREA &~ (1 << 9);
    mes l("Reset!");
    close;

L_Close:
    //showavatar;  // Use this to hide the showavatar
    close;

// Use @shownpc to enable ZegasDebug
OnInit:
    if (!debug)
        disablenpc "ZegasDebug";
    end;
}
