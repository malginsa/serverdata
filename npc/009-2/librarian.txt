// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Main Storyline
//      NPC_SAMANTHA

009-2,67,60,2	script	Book Keeper	NPC_FEMALE,{
    .@n = getq(General_Narrator);
    if (.@n == 9) mesn;
    if (.@n == 9) mesc l("Anyway, I think you should go to Hurnscald Townhall again, and speak with the mayor.");
    if (.@n == 9) next;
    if (.@n >= 9) goto L_Complete;
    if (.@n == 8) goto L_Report;
    if (.@n == 6 || .@n == 7) goto L_Advance;
    if (.@n < 6 && .@n >= 2) goto L_Horses;
    mesn;
    mesq l("Hello. I am the book keeper, responsible for keeping Halinarzo History.");
    // TODO: Import data from BR-002-5 (Royal Library) and make available story about TMW-BR
    close;

L_Horses:
    mesn;
    mesq l("Hello. I am the book keeper, responsible for keeping Halinarzo History.");
    next;
    select
        l("Cool, thank you."),
        l("I was born here but I had amnesia.");
    mes "";
    if (@menu == 1)
        close;
    mesn;
    mesq l("Please prove your statement.");
    next;
    mesc l("You realize that you have no means to prove your statement.");
    mesc l("Saying that you were sent here would prove nothing.");
    mesc l("How do you even prove amnesia, anyway?");
    next;
    mesn;
    mesq l("You are just a random stranger trying to steal adventurer data, aren't you?? %%e");
    next;
    mesn;
    mesq l("I bet you'll now ask for %s's password, and scam all their money!!", strcharinfo(0));
    next;
    mesn;
    mesq l("Get out of here! Before I call the city guard!!");
    close;

L_Advance:
    .@nt = getq2(General_Narrator);
    // Initial Value is 0.
    // .@nt tracks future value
    if (!.@nt || .@n == 6)
        goto L_Prologue;

    // If you wait 7 - instead of 2 - hours, you are abusing.
    // So now, you must wait an extra hour.
    if (.@nt > santime()+(60*60*7)) {
        setq2 General_Narrator, santime()+(60*60);
        .@nt=santime()+(60*60);
    }

    // You got magic, so, why waiting so long. Chop half hour.
    // Should be plenty to instantly finish.
    if (MAGIC_LVL && !@halinskip) {
        .@nt-=1800;
        setq2 General_Narrator, .@nt;
        @halinskip=true;
    }

    // Wait time is over!
    if (.@nt <= santime())
        goto L_Complete;

    mesn;
    mesq l("Have you found out magic already? It should not be possible, but the girl reading the book says it is. I don't know in what to believe.");
    next;
    mesn;
    mesq l("Anyway, I still need @@. You have plenty of time to do things at your own pace.", FuzzyTime(.@nt));
    // Rule #99999999: Never tell players to grind for 3 hours you lazy dev
    //mesq l("Please help us with the bandits on the Canyon, and meanwhile, I'll seek the records for you. Just @@ more.", FuzzyTime(.@nt));
    close;

L_Prologue:
    mesn strcharinfo(0);
    mesq l("Hi! Apparently, I came from here and moved at the age of 4, but I had amnesia and can't remember!");
    next;
    mesn strcharinfo(0);
    mesq l("It would be very kind of you, if you could seek who I am!");
    next;
    mesn;
    mesq l("What are you talking about? You are who you are right now. There's no other you.");
    mesc l("Unless, of course, if Saulc cloned you. But then we must blame Saulc!");
    next;
    mesn;
    mesq l("Ah... I see. You are a lost soul, without parents, lost on the world with only some basic stuff.");
    next;
    mesn;
    mesq l("Alright, I'll look in the archives. I'll have an answer for you in @@. Meanwhile, why don't you suppress the bandits on the cliff?", l("45 minutes"));
    // Please wait 45 minutes
    setq General_Narrator, 7, santime()+(60*45);
    next;
    mesn;
    mesq l("Also, Halinarzo is famous for the depleted mana mines in the town. You probably won't find a mana stone there, but it might be cool to look.");
    close;

L_Complete:
    mesn;
    mesq l("So, I looked the records. There was nothing really useful.");
    mesq lg("Like, I know you are female.", "Like, I know you are male.");
    next;
    mesn;
    mesq l("But, I found something really interesting! Ah... I hope you like history?");
    next;
    mesn l("HISTORY BOOK");
    mesc l("In these dark times, countless souls moved away from Halinarzo. All hope seemed lost.");
    mesc l("But then, Andrei Sakar appeared. Hope was not lost. We could still defend it.");
    next;
    mesn l("HISTORY BOOK");
    mesc l("Many sacrifices were done, but it was lost.");
    mesc l("Even the mighty Andrei Sakar could only barely escape alive. The Monster King was too strong.");
    mesc l("All hope was lost. We failed to protect it. We lost everyone who challenged the Monster King. Killed without mercy.");
    next;
    mesn l("HISTORY BOOK");
    mesc l("The Monster King was so powerful! It is impossible to recover the artifact now, and everyone will die!");
    mesc l("A few families, also known as the Ancient Families of the Soul Menhir (for whatever reasons that may be), departed.");
    next;
    mesn l("HISTORY BOOK");
    mesc l("They promised to travel to the ##BWorld Edge##b to fetch a Mana Fragment. The wisest of them said it was the only chance.");
    mesc l("They were never again seen. None of them. We burnt all records about everyone here, and blamed the Monster King.");
    mesc l("They must never know we had it. Their sacrifices must be forgotten. For the sake of all.");
    compareandsetq General_Narrator, 7, 8;
    close;

L_Report:
    mesn;
    mesq l("Do you want to read the story again? Or should we get to the most obvious point?");
    next;
    select
        l("I want to know what you found out again"),
        l("I want to know why you shown me that.");
    if (@menu == 1)
        goto L_Complete;
    mesn;
    mesq l("We have three vanishing sort of people on the story.");
    next;
    mesn;
    mes l("1- The coward families, who ran away when outlook was bad.");
    mes l("2- The defenders of the artifact, who lost their lives and left people behind.");
    mes l("3- The ancient families.");
    next;
    mesn;
    mesq l("You probably was only 4 when all that happened. And opposed to official story, the Monster King never enter this building.");
    next;
    mesn;
    mesq l("In other words: If Lua couldn't find your record, your parents were part of this story! But, on which of these three groups were them?");
    next;
    mesn;
    mes l("Well, if you were from the defenders, you would have been adopted by a family which left Halinarzo.");
    mesq l("And if your family, real or adoptive, was coward, there would be no lead at all.");
    next;
    mesn;
    mesq l("So, let's work with the last proposal, that you're from an ancient family. We can backtrace what they did.");
    next;
    mesn;
    mesq l("Their first stop was Hurnscald, so please go to Hurnscald Townhall, and speak with the mayor, he can help you getting there. You can't reach there normally, after all.");
    next;
    inventoryplace ScholarshipBadge, 1;
    mesn;
    mesq l("Good luck, @@!", strcharinfo(0));
    mesc l("@@ hands you an @@.", .name$, getitemlink(ScholarshipBadge));
    next;
    getexp BaseLevel*750, JobLevel*50;// Reference Levels: (40, 15)
    getitem ScholarshipBadge, 1;
    mesc b(l(".:: Main Quest 4-1 ::.")), 3;
    mesc l("* Talk to Librarian in Halinarzo"), 2;
    mesc l("* Return to Hurnscald Townhall"), 9;
    setq General_Narrator, 9;
    getvaultexp(10);
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, FancyHat);
    setunitdata(.@npcId, UDT_HEADMIDDLE, CreasedShirt);
    //setunitdata(.@npcId, UDT_HEADBOTTOM, NPCEyes);
    setunitdata(.@npcId, UDT_WEAPON, JeansShorts);
    setunitdata(.@npcId, UDT_HAIRSTYLE, any(8,11,20));
    setunitdata(.@npcId, UDT_HAIRCOLOR, 5);
    npcsit;

    .sex=G_FEMALE;
    .distance=5;
    end;
}

