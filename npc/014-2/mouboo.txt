// TMW2/LoF scripts.
// Authors:
//    TMW-LoF Team
//    Jesusalva
// Description:
//    Part from a bigger, very important quest for learning about the MOUBOOTAUR.
//    Interacts with Sagratha (for uncursing).
//
//    Quest Structure:
//      HurnscaldQuest_InjuriedMouboo - STATUS - HPBAR
//    Quest Status:
//      0 - First Met
//      1 - Examined (HP Bar stored on field 2)
//      2 - Healed (Curse not solved - ready for Sagratha Quest)
//      3 - Curse removed (Sagratha's Arc)
//      9 - KILLED YOUR MOUBOO. YOU EVIL MONSTER.

014-2,144,80,0	script	Mouboo#0142injuried	NPC_INJURIED_MOUBOO,{
    .@q=getq(HurnscaldQuest_InjuriedMouboo);

    if (.@q < 2) goto L_Begin;
    if (.@q == 2) goto L_Healed;
    if (.@q == 3) goto L_Uncursed;
    if (.@q == 9) goto L_Killed;
    goodbye;
    end;

// Main Quest
L_Begin:
    .@q=getq(HurnscaldQuest_InjuriedMouboo);
    .@q2=getq2(HurnscaldQuest_InjuriedMouboo);
    mesn l("Injured Mouboo");
    mesq l("Booo....");
    mesc l("The Mouboo seems to be lying in pain.");
    next;
    select
        rif(.@q == 1, l("Attempt to heal the Mouboo")),
        rif(.@q == 1, l("Attempt to kill the Mouboo")),
        l("Examine the Mouboo"),
        l("Leave the Mouboo alone");
    mes "";
    switch (@menu) {
        case 3:
            mesc l("The Mouboo is lying down on the grass.");
            mesc l("There doesn't seem to be any physical wound.");
            mesc l("A closer inspection suggests this mouboo has been... CURSED.");
            if (.@q == 0)
                setq HurnscaldQuest_InjuriedMouboo, 1, 100;
            next;
            goto L_Begin;
        case 1:
            mes "##B" + l("Drag and drop a healing item from your inventory.") + "##b";

            .@id = requestitem();

            // If ID is invalid, there's not enough items, it is bound = Cannot bury
            if (.@id < 1) close;
            if (.@id < 1 || countitem(.@id) < 1 || checkbound(.@id) || getiteminfo(.@id, ITEMINFO_TYPE) != IT_HEALING) {
                if (checkbound(.@id))
                    mesc l("You cannot part with this item!");
                else if (.@id == MoubooFigurine)
                    mesc l("Haha, this wooden figurine won't do the job, sorry.");
                else if (.@id == PlushMouboo)
                    mesc l("This seems like a good idea at first, but actually, it isn't.");
                else if (.@id == MoubooPendant)
                    mesc l("Wise choice! But can you really part with it? Better think on something else!");
                else if (.@id == MoubooHat || .@id == AlphaMoubooHat)
                    mesc l("...I don't think giving it this hat will really help...");
                else if (.@id == DeathPotion || .@id == NymphPoison)
                    mesc l("...Are you crazy?");
                else if (getiteminfo(.@id, ITEMINFO_TYPE) != IT_HEALING)
                    mesc l("This is not a healing item.");
                else
                    mesc l("You give up.");
                close;
            }

            // Bad healing items
            if (.@id == MoubooSteak) {
                mesc l("Uhm, this seems like a bad idea.");
                close;
            } else if (.@id == DeathPenalty) {
                mesc l("Uhm, that's an... @@. Do you... want to grow a tree on the mouboo or something? Can't you think on something else?", getitemlink(DeathPenalty));
                close;
            } else if (.@id == BottleOfSewerWater) {
                mesc l("Uhm... I thought you were trying to heal the mouboo. Why are you giving it poisonous stuff?!");
                close;
            } else if (.@id == Coffee) {
                mesc l("Uhm, I better heal it first before trying to do anything about the curse.");
                close;
            }

            mesc l("Really give your @@ to the Mouboo?", getitemlink(.@id)), 1;
            mesc l("The item will be lost forever.");
            next;
            if (askyesno() == ASK_YES) {
                delitem .@id, 1;
                itemeffect(.@id); // As a bonus, you'll be healed too, because I need to retrieve @max value

                // You need about 9.000 HP-worth healing items, and you can't use Mouboo Steak
                @val1*=rand2(3,5); // Average delay (FIXME: Is @val1 available?)
                if (.@id == ElixirOfLife)
                    setq2 HurnscaldQuest_InjuriedMouboo, 9999;
                else
                    setq2 HurnscaldQuest_InjuriedMouboo, .@q2+((@val1)/10);

                // Get rid of temporary variables, if it haven't happened yet
                @min = 0;
                @max = 0;
                @val1 = 0;

                .@q2=getq2(HurnscaldQuest_InjuriedMouboo);
                if (.@q2 > 1000) {
                    mesc l("This will be remembered."), 6;
                    setq HurnscaldQuest_InjuriedMouboo, 2;
                    getexp 800, 0;
                    close;
                }

                // Progress Report
                if (.@q2 > 900) {
                    mesc l("The mouboo seems to want to thank you, but is still too weak. Let's finish this healing.");
                }
                else if (.@q2 > 700) {
                    mesc l("The mouboo looks a bit more healthy. A little more should do.");
                }
                else if (.@q2 > 400) {
                    mesc l("The mouboo looks a bit less sickly. But this is not enough.");
                }
                else if (.@q2 > 100) {
                    mesc l("The mouboo still looks terrible, but you can see a small difference.");
                }
                else {
                    // Mouboo begins at 100 HP, so this should never happen
                    mesc l("Barely any difference, maybe I should use better healing items on it.");
                }
            }
            break;
        case 2:
            .@id = getequipid(EQI_HAND_R);

            // If ID is invalid, there's not enough items, it is bound = Cannot bury
            if (.@id < 1) {
                mesc l("You are not using a weapon for a merciful last blow.");
                close;
            }

            mesc l("Really attack the Mouboo with a(n) @@?", getitemlink(.@id)), 1;
            mesc l("There will be consequences.");
            next;
            if (askyesno() == ASK_YES) {
                .@atk=getiteminfo(.@id, ITEMINFO_ATK);
                mesn l("Injured Mouboo");
                if (.@q2 < 20)
                    mesq l("Re-ref... Reflect.");
                else
                    mesq l("Ma... Magical Re... Re-ref... Reflection.");
                setq2 HurnscaldQuest_InjuriedMouboo, .@q2-(.@atk/5);
                .@q2=getq2(HurnscaldQuest_InjuriedMouboo);

                if (.@q2 <= 0) {
                    mes l("The... Mou... Boo... Taur...");
                    mes l("Is... Dan... Ger... Ous...");
                    mes l("...Help... Me...");
                    setq HurnscaldQuest_InjuriedMouboo, 9;
                    getexp 800, 0;
                    next;

                    mesc l("This will be remembered."), 6;
                    heal -.@atk, -(.@atk/10);
                    close;
                }

                heal -.@atk, -(.@atk/10);
                next;
            }
            break;
        default:
            close;
    }
    mes "";
    goto L_Begin;


// Sagratha Arc
L_Healed:
    .@q=getq(HurnscaldQuest_InjuriedMouboo);
    .@q2=getq2(HurnscaldQuest_InjuriedMouboo);
    mesn l("Injuried Mouboo");
    mesq l("Zzzzzz....");
    mesc l("The mouboo is sleeping soundly. The curse is still there.");
    if (countitem(Coffee)) {
        next;
        mesc l("Give a @@ to the Mouboo?", getitemlink(Coffee));
        next;
        if (askyesno() == ASK_YES) {
            mesn l("Injured Mouboo");
            mesc l("*screams*");
            mes "";
            mesc l("The Mouboo doesn't want to drink the Coffee. Try to force it to drink anyway?");
            next;
            if (askyesno() == ASK_YES) {
                delitem Coffee, 1;
                mesc l("There's no change in the condition of the poor Mouboo.");
                next;
                mesc l("Maybe the Hurnscald alchemist, Wyara, could explain why.");
            } else {
                mesc l("Coffee is good, why does the Mouboo refuse to drink it?");
                mesc l("Maybe someone in Hurnscald can help me.");
            }
        }
    }
    close;

// Final Results
L_Uncursed:
    mesn;
    mesq l("Hello, @@. Thanks for helping me.", strcharinfo(0));
    next;
    mesn;
    mesq l("Don't challenge the Moubootaur. You saw what happened to me.");
    close;

L_Killed:
    mesc l("Did this Mouboo just blink? No, I made sure it was truly dead. Must have been my imagination.");
    close;

OnInit:
    .sex=G_OTHER;
    .distance=2;
    end;
}
